/*
 Navicat Premium Data Transfer

 Source Server         : PGADMIN
 Source Server Type    : PostgreSQL
 Source Server Version : 90514
 Source Host           : localhost:5432
 Source Catalog        : bapsi
 Source Schema         : public

 Target Server Type    : PostgreSQL
 Target Server Version : 90514
 File Encoding         : 65001

 Date: 29/01/2019 22:33:29
*/

CREATE SEQUENCE IF NOT EXISTS db_rekognisi_rekognisi_id_seq
    INCREMENT BY 1
    MINVALUE 1
    MAXVALUE 987654321
    START WITH 1;
    
-- ----------------------------
-- Table structure for db_rekognisi
-- ----------------------------
DROP TABLE IF EXISTS "public"."db_rekognisi";
CREATE TABLE "public"."db_rekognisi" (
  "rekognisi_id" int2 NOT NULL DEFAULT nextval('db_rekognisi_rekognisi_id_seq'::regclass),
  "rekognisi_tahunakademik" varchar(255) COLLATE "pg_catalog"."default",
  "rekognisi_rekognisi" text COLLATE "pg_catalog"."default",
  "rekognisi_tempat" text COLLATE "pg_catalog"."default",
  "rekognisi_tglkegiatan" date,
  "rekognisi_file" text COLLATE "pg_catalog"."default",
  "rekognisi_idpeg" int2
)
;

-- ----------------------------
-- Records of db_rekognisi
-- ----------------------------
INSERT INTO "public"."db_rekognisi" VALUES (2, '18.1', 'ACARA SEMINAR KEBANGSAAN', 'AKPRIND', '2029-01-27', '955691c785ffa5e9c0c7a4a9dfc63f61.pdf', 248);
