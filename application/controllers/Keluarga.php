<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Keluarga extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -  
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in 
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see http://codeigniter.com/user_guide/general/urls.html
	 */
	function __construct(){
		parent::__construct();
		$this->load->Model(array('crud'));
		if(!$this->session->userdata('login')){
			redirect(site_url('Login'));
		}		
	}
	private $path="../simpeg/fileijazah/";
	//private	$path='./uploadfile/';

	public function alertmessage(){
		$data=array(
			'simpansuccess'=>'Data berhasil disimpan',
			'simpanerror'=>'Data tidak berhasil disimpan',
			'hapussuccess'=>'Hapus berhasil',
			'hapuserror'=>'Hapus gagal',
			);
		return $data;
	}
	public function fileupload($path,$file){
			$config=array(
				'upload_path'=>$path,
				'allowed_types'=>'pdf',
				'max_size'=>5000,
				'encrypt_name'=>true,
			);
			$this->load->library('upload',$config);
			return $this->upload->do_upload($file);
		}
	public function downloadfile($file){
			$link=$this->path.$file;	
			if(file_exists($link)){
				$url=file_get_contents($link);
				force_download($file,$url);
			}else{
				$this->session->set_flashdata('error','File tidak ditemukan');
				redirect(site_url("User/riwayatpendidikan"));	
			}	
								
		}			
	public function index()
	{
		redirect(site_url('User/keluarga'));
	}
	public function simpan_data(){
		$alert=$this->alertmessage();
		$data=array(
			'anggota'=>$this->input->post('anggota'),
			'hubungan'=>$this->input->post('hubungan'),
			'tgl_lahir'=>date('Y-m-d',strtotime($this->input->post('tgl_lahir'))),
			'telp'=>$this->input->post('telp'),
			'alamat'=>$this->input->post('alamat'),
			'pekerjaan'=>$this->input->post('pekerjaan'),
			'kotakelahiran'=>$this->input->post('kotakelahiran'),
			'idpeg'=>$this->input->post('idpeg'),
		);
		//JIKA TGL MENIKAH DAN ANAK DI ISI
		if($this->input->post('anak_ke')){
			$data['anak_ke']=$this->input->post('anak_ke');
		}elseif($this->input->post('tgl_menikah')){
			$data['tgl_menikah']=date('Y-m-d',strtotime($this->input->post('tgl_menikah')));
		}
		$query=array(
			'data'=>$data,
			'tabel'=>"db_keluarga1",
		);
		$res=$this->crud->insert($query);
		if($res==1){
			$this->session->set_flashdata('success',$alert['simpansuccess']);
		}else{
			$this->session->set_flashdata('error',$res);
		}
		redirect(site_url('User/datakeluarga'));
		//print_r($data);
	}
	function hapus($id){	
		$alert=$this->alertmessage();
		$query=array(
			'where'=>array('id'=>$id),
			'tabel'=>'db_keluarga1',
		);		
		$res=$this->crud->delete($query);
		if($res==1){
			$this->session->set_flashdata('success',$alert['hapussuccess']);
		}else{
			$this->session->set_flashdata('error',$res);
		}
		redirect(site_url('User/datakeluarga'));
	}
	function edit_data(){
		$id=$this->input->post('id');
		if($this->input->post('submit')){
			$data=array(
				'anggota'=>$this->input->post('anggota'),
				'hubungan'=>$this->input->post('hubungan'),
				'tgl_lahir'=>date('Y-m-d',strtotime($this->input->post('tgl_lahir'))),
				'telp'=>$this->input->post('telp'),
				'alamat'=>$this->input->post('alamat'),
				'pekerjaan'=>$this->input->post('pekerjaan'),
				'kotakelahiran'=>$this->input->post('kotakelahiran'),
			);
			//JIKA TGL MENIKAH DAN ANAK DI ISI
			if($this->input->post('anak_ke')){
				$data['anak_ke']=$this->input->post('anak_ke');
			}elseif($this->input->post('tgl_menikah')){
				$data['tgl_menikah']=date('Y-m-d',strtotime($this->input->post('tgl_menikah')));
			}

			$query=array(
				'where'=>array('id'=>$id),
				'data'=>$data,
				'tabel'=>'db_keluarga1',
			);
			$res=$this->crud->update($query);
			$alert=$this->alertmessage();
			if($res==1){
				$this->session->set_flashdata('success',$alert['simpansuccess']);
			}else{
				$this->session->set_flashdata('error',$res);
			}
			redirect(site_url('User/datakeluarga'));
		}else{
			$query=array(
				'where'=>array('id'=>$id),
				'tabel'=>'db_keluarga1',
			);
			$data=array(
				'data'=>$this->crud->read($query)->row(),
				);
			$this->load->view('keluarga/edit',$data);			
		}
		//print_r($data['data']);
	}
	function detail_data(){
		$id=$this->input->post('id');
		$query=array(
			'where'=>array('id'=>$id),
			'tabel'=>"db_keluarga1",
		);
		$data=array(
			'data'=>$this->crud->read($query)->row(),
		);
		$this->load->view('keluarga/detail',$data);
	}
}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */