<?php
	Class Laporan extends CI_Controller{
		function __construct(){
			parent::__construct();
			$this->idpegawai=$this->session->userdata('id_pegawai');
			$this->load->model(array('mseminarpenelitian','mtahunakad','Crud'));
			if($this->session->userdata('login')!=true){
				redirect(site_url('Login/logout'));
			}				
		}
		//DEKLARASI VAR
		//private $master_tabel='serdos';
		private $msg_simpansuccess="Data berhasil disimpan";
		private $msg_hapussuccess="Data berhasil dihapus";
		private $default_url='Laporan';

		function fileupload($path,$file){
			$config=array(
				'upload_path'=>$path,
				'allowed_types'=>'pdf',
				'max_size'=>5000, //5mb
				'encrypt_name'=>true,
			);
			$this->load->library('upload',$config);
			return $this->upload->do_upload($file);
		}			
		function index(){		
			redirect(site_url($this->default_url.'/serkom'));
		}
		public function serkom(){
			$query=array(
				'select'=>'b.idpeg,b.gelardepan,b.nama,b.gelarbelakang,count(a.serkom_id) AS jumlah',
				'tabel'=>'serkom a',
				'join'=>array(array('tabel'=>'db_pegawai b','ON'=>'a.serkom_idpegawai=b.idpeg','jenis'=>'RIGHT')),
				'where'=>array(array('b.kategori'=>'pendidik')),
				'groupby'=>array('b.nama','b.idpeg','b.gelardepan','b.gelarbelakang'),
				'order'=>array('kolom'=>'jumlah','orderby'=>'DESC'),				
			);	
			$tendik=array(
				'select'=>'b.idpeg,b.gelardepan,b.nama,b.gelarbelakang,count(a.serkom_id) AS jumlah',
				'tabel'=>'serkom a',
				'join'=>array(array('tabel'=>'db_pegawai b','ON'=>'a.serkom_idpegawai=b.idpeg','jenis'=>'RIGHT')),
				'where'=>array(array('b.kategori'=>'kependidikan')),
				'groupby'=>array('b.nama','b.idpeg','b.gelardepan','b.gelarbelakang'),
				'order'=>array('kolom'=>'jumlah','orderby'=>'DESC'),				
			);							
			$data=array(
				'menu'=>'laporan',
				'submenu'=>'laporanserkom',
				'headline'=>'laporanserkom',
				'judul'=>'laporan serkom',
				'dosen'=>$this->Crud->join($query)->result(),
				'tendik'=>$this->Crud->join($tendik)->result(),
			);
			$this->load->view('administrator',$data);				
			//print_r($data['tendik']);			
		}
		public function serdos(){
			$query=array(
				'select'=>'b.idpeg,b.gelardepan,b.nama,b.gelarbelakang,count(a.serdos_id) AS jumlah',
				'tabel'=>'serdos a',
				'join'=>array(array('tabel'=>'db_pegawai b','ON'=>'a.serdos_idpegawai=b.idpeg','jenis'=>'RIGHT')),
				'where'=>array(array('b.kategori'=>'pendidik')),
				'groupby'=>array('b.nama','b.idpeg','b.gelardepan','b.gelarbelakang'),
				'order'=>array('kolom'=>'jumlah','orderby'=>'DESC'),				
			);				
			$data=array(
				'menu'=>'laporan',
				'submenu'=>'laporanserdos',
				'headline'=>'laporanserdos',
				'judul'=>'laporan serdos',
				'dosen'=>$this->Crud->join($query)->result(),
			);
			$this->load->view('administrator',$data);				
			//print_r($data['dosen']);			
		}
		public function seminartendik(){
			$query=array(
				'select'=>'b.idpeg,b.gelardepan,b.nama,b.gelarbelakang,count(a.seminar_id) AS jumlah',
				'tabel'=>'db_seminartendik a',
				'join'=>array(array('tabel'=>'db_pegawai b','ON'=>'a.seminar_idpeg=b.idpeg','jenis'=>'RIGHT')),
				'where'=>array(array('b.kategori'=>'kependidikan')),
				'groupby'=>array('b.nama','b.idpeg','b.gelardepan','b.gelarbelakang'),
				'order'=>array('kolom'=>'jumlah','orderby'=>'DESC'),				
			);				
			$data=array(
				'menu'=>'laporan',
				'submenu'=>'laporanseminartendik',
				'headline'=>'laporanseminartendik',
				'judul'=>'laporan seminar tendik',
				'dosen'=>$this->Crud->join($query)->result(),
			);
			$this->load->view('administrator',$data);				
			//print_r($data['dosen']);			
		}
		public function rekognisi(){
			$query=array(
				'select'=>'b.idpeg,b.gelardepan,b.nama,b.gelarbelakang,count(a.rekognisi_id) AS jumlah',
				'tabel'=>'db_rekognisi a',
				'join'=>array(array('tabel'=>'db_pegawai b','ON'=>'a.rekognisi_idpeg=b.idpeg','jenis'=>'RIGHT')),
				'where'=>array(array('b.kategori'=>'pendidik')),
				'groupby'=>array('b.nama','b.idpeg','b.gelardepan','b.gelarbelakang'),
				'order'=>array('kolom'=>'jumlah','orderby'=>'DESC'),				
			);				
			$data=array(
				'menu'=>'laporan',
				'submenu'=>'laporanrekognisi',
				'headline'=>'laporanrekognisi',
				'judul'=>'laporan rekognisi',
				'dosen'=>$this->Crud->join($query)->result(),
			);
			$this->load->view('administrator',$data);				
		}	
		public function usiadanpensiun(){
			$query=array(
				'tabel'=>'db_pegawai',
				'select'=>'idpeg,nama,gelardepan,gelarbelakang,tanggallahir,awaltetap,awalmasuk,statuspegawai',
				'order'=>array('kolom'=>'nama','orderby'=>'ASC'),
				'where'=>array(array()),
			);
			$pegawai=$this->Crud->read($query)->result();
			$arrayakhir=array();
	 	 
			if(count($pegawai)<>0){
				foreach ($pegawai as $index => $row) {
					$arrayakhir[$index]=$row;
					$tgltetap = new DateTime($row->awaltetap);
					$tglmasuk = new DateTime($row->awalmasuk);
					$tgllahir = new DateTime($row->tanggallahir);
					$today = new DateTime();
					
					$umur=$today->diff($tgllahir);
					$mkg=$today->diff($tgltetap);
					$mkk=$today->diff($tglmasuk);
		
					$arrayakhir[$index]->umur=array('tahun'=>$umur->y,'bulan'=>$umur->m);					
					$arrayakhir[$index]->mkg=array('tahun'=>$mkg->y,'bulan'=>$mkg->m);	
					$arrayakhir[$index]->mkk=array('tahun'=>$mkk->y,'bulan'=>$mkk->m);	
				}			
			}		
				$data=array(
					'menu'=>'laporan',
					'submenu'=>'laporanusiadanpensiun',
					'headline'=>'laporanusiadanpensiun',
					'judul'=>'laporan usia dan pensiun',
					'data'=>$arrayakhir,
				);
				$this->load->view('administrator',$data);				
		}		
		public function jabatanfungsional(){
			$query=array(
				'select'=>'b.idpeg,b.gelardepan,b.nama,b.gelarbelakang,namajafa,angkakredit,file',
				'tabel'=>'db_jafa a',
				'join'=>array(array('tabel'=>'db_pegawai b','ON'=>'a.idpeg=b.idpeg','jenis'=>'RIGHT')),
				'where'=>array(array('b.kategori'=>'pendidik')),
				'groupby'=>array(),
				'order'=>array('kolom'=>'a.idpeg','orderby'=>'ASC'),				
			);
			$pegawai=array(
				'select'=>'a.idpeg,a.nama,a.gelardepan,a.gelarbelakang',
				'tabel'=>'db_pegawai a',
				'where'=>array(array('kategori'=>'pendidik')),
				'order'=>array('kolom'=>'nama','orderby'=>'ASC'),
			);
			$dtpegawai=$this->Crud->read($pegawai)->result();
			$res=array();
			foreach ($dtpegawai as $index => $row) {
				$res[$index]=$row;
				$res[$index]->idjafa='';
				$res[$index]->namajafa='';
				$res[$index]->angkakredit='';
				$res[$index]->tmt='';
				$query2="SELECT a.idpeg,a.nama,b.id AS idjafa,b.namajafa,b.angkakredit,b.tmt FROM db_pegawai a 
						JOIN db_jafa b ON b.idpeg=a.idpeg WHERE a.idpeg=$row->idpeg
						ORDER BY b.tmt DESC LIMIT 1";
				$result2=$this->Crud->hardcode($query2)->row();				
				if($result2){
					$res[$index]->idjafa=$result2->idjafa;
					$res[$index]->namajafa=$result2->namajafa;
					$res[$index]->angkakredit=$result2->angkakredit;	
					$res[$index]->tmt=$result2->tmt;
				}else{}
				//$res=$res;
			}
			$data=array(
				'menu'=>'laporan',
				'submenu'=>'laporanjabatanfungsional',
				'headline'=>'laporanjabatanfungsional',
				'judul'=>'laporan jabatan fungsional',
				//'dosen'=>$this->Crud->join($query)->result(),
				'dosen'=>$res,
			);
			// print_r($res);
			// exit();
			$this->load->view('administrator',$data);
		}
		public function jabatanprofesi(){
			$pegawai=array(
				'select'=>'a.idpeg,a.nama,a.gelardepan,a.gelarbelakang',
				'tabel'=>'db_pegawai a',
				'where'=>array(array('kategori'=>'pendidik')),
				'order'=>array('kolom'=>'nama','orderby'=>'ASC'),
			);	
			$dtpegawai=$this->Crud->read($pegawai)->result();
			$res=array();
			foreach ($dtpegawai as $index => $row) {
				$res[$index]=$row;
				$res[$index]->nomorsk='';
				$res[$index]->namagol='';
				$res[$index]->tmt='';
				$res[$index]->file='';
				$res[$index]->idjabatan='';
				$query2="SELECT a.idpeg,a.nama,b.* FROM db_pegawai a 
						JOIN db_riwayatjabatan b ON b.idpeg=a.idpeg WHERE a.idpeg=$row->idpeg
						ORDER BY b.tmt DESC LIMIT 1";
				$result2=$this->Crud->hardcode($query2)->row();				
				if($result2){
					$res[$index]->nomorsk=$result2->nomorsk;
					$res[$index]->namagol=$result2->namagol;
					$res[$index]->tmt=$result2->tmt;
					$res[$index]->file=$result2->file;
					$res[$index]->idjabatan=$result2->id;
				}else{}
				//$res=$res;
			}					
			$data=array(
				'menu'=>'laporan',
				'submenu'=>'laporanjabatanprofesi',
				'headline'=>'laporanjabatanprofesi',
				'judul'=>'laporan jabatan profesi',
				'dt'=>$res,
			);
			//print_r($data);
			$this->load->view('administrator',$data);
		}
		//TRI DHARMA
		public function penelitian(){
			$query=array(
				'select'=>'b.idpeg,b.gelardepan,b.nama,b.gelarbelakang,count(a.id) AS jumlah',
				'tabel'=>'db_penelitian a',
				'join'=>array(array('tabel'=>'db_pegawai b','ON'=>'a.idpeg=b.idpeg','jenis'=>'RIGHT')),
				'where'=>array(array('b.kategori'=>'pendidik')),
				'groupby'=>array('b.nama','b.idpeg','b.gelardepan','b.gelarbelakang'),
				'order'=>array('kolom'=>'jumlah','orderby'=>'DESC'),				
			);
			$data=array(
				'menu'=>'tridharma',
				'submenu'=>'penelitian',
				'headline'=>'penelitian',
				'judul'=>'laporan penelitian',
				'data'=>$this->Crud->join($query)->result(),
			);			
			//print_r($data);		
			$this->load->view('administrator',$data);		
		}
		public function jurnal(){
			$query=array(
				'select'=>'b.idpeg,b.gelardepan,b.nama,b.gelarbelakang,count(a.id) AS jumlah',
				'tabel'=>'db_jurnal a',
				'join'=>array(array('tabel'=>'db_pegawai b','ON'=>'a.idpeg=b.idpeg','jenis'=>'RIGHT')),
				'where'=>array(array('b.kategori'=>'pendidik')),
				'groupby'=>array('b.nama','b.idpeg','b.gelardepan','b.gelarbelakang'),
				'order'=>array('kolom'=>'jumlah','orderby'=>'DESC'),				
			);
			$data=array(
				'menu'=>'tridharma',
				'submenu'=>'jurnal',
				'headline'=>'jurnal',
				'judul'=>'laporan jurnal',
				'data'=>$this->Crud->join($query)->result(),
			);			
			//print_r($data);		
			$this->load->view('administrator',$data);		
		}
		public function abdimas(){
			$query=array(
				'select'=>'b.idpeg,b.gelardepan,b.nama,b.gelarbelakang,count(a.id) AS jumlah',
				'tabel'=>'db_jurnal a',
				'join'=>array(array('tabel'=>'db_pegawai b','ON'=>'a.idpeg=b.idpeg','jenis'=>'RIGHT')),
				'where'=>array(array('b.kategori'=>'pendidik')),
				'groupby'=>array('b.nama','b.idpeg','b.gelardepan','b.gelarbelakang'),
				'order'=>array('kolom'=>'jumlah','orderby'=>'DESC'),				
			);
			$data=array(
				'menu'=>'tridharma',
				'submenu'=>'abdimas',
				'headline'=>'abdimas',
				'judul'=>'laporan abdimas',
				'data'=>$this->Crud->join($query)->result(),
			);			
			//print_r($data);		
			$this->load->view('administrator',$data);		
		}
		public function seminar(){
			$query=array(
				'select'=>'b.idpeg,b.gelardepan,b.nama,b.gelarbelakang,count(a.id) AS jumlah',
				'tabel'=>'db_seminar a',
				'join'=>array(array('tabel'=>'db_pegawai b','ON'=>'a.idpeg=b.idpeg','jenis'=>'RIGHT')),
				'where'=>array(array('b.kategori'=>'pendidik')),
				'groupby'=>array('b.nama','b.idpeg','b.gelardepan','b.gelarbelakang'),
				'order'=>array('kolom'=>'jumlah','orderby'=>'DESC'),				
			);
			$data=array(
				'menu'=>'tridharma',
				'submenu'=>'seminardosen',
				'headline'=>'seminardosen',
				'judul'=>'laporan seminar',
				'data'=>$this->Crud->join($query)->result(),
			);			
			//print_r($data);		
			$this->load->view('administrator',$data);		
		}
		public function haki(){
			$query=array(
				'select'=>'b.idpeg,b.gelardepan,b.nama,b.gelarbelakang,count(a.id) AS jumlah',
				'tabel'=>'db_haki a',
				'join'=>array(array('tabel'=>'db_pegawai b','ON'=>'a.idpeg=b.idpeg','jenis'=>'RIGHT')),
				'where'=>array(array('b.kategori'=>'pendidik')),
				'groupby'=>array('b.nama','b.idpeg','b.gelardepan','b.gelarbelakang'),
				'order'=>array('kolom'=>'jumlah','orderby'=>'DESC'),				
			);
			$data=array(
				'menu'=>'tridharma',
				'submenu'=>'haki',
				'headline'=>'haki',
				'judul'=>'laporan hak kekayaan intelektual',
				'data'=>$this->Crud->join($query)->result(),
			);			
			//print_r($data);		
			$this->load->view('administrator',$data);		
		}
		public function buku(){
			$query=array(
				'select'=>'b.idpeg,b.gelardepan,b.nama,b.gelarbelakang,count(a.id) AS jumlah',
				'tabel'=>'db_buku a',
				'join'=>array(array('tabel'=>'db_pegawai b','ON'=>'a.idpeg=b.idpeg','jenis'=>'RIGHT')),
				'where'=>array(array('b.kategori'=>'pendidik')),
				'groupby'=>array('b.nama','b.idpeg','b.gelardepan','b.gelarbelakang'),
				'order'=>array('kolom'=>'jumlah','orderby'=>'DESC'),				
			);
			$data=array(
				'menu'=>'tridharma',
				'submenu'=>'buku',
				'headline'=>'buku',
				'judul'=>'laporan penulisan buku',
				'data'=>$this->Crud->join($query)->result(),
			);			
			//print_r($data);		
			$this->load->view('administrator',$data);		
		}	
		public function organisasi(){
			// $query=array(
			// 	'select'=>'b.idpeg,b.gelardepan,b.nama,b.gelarbelakang,count(a.id) AS jumlah',
			// 	'tabel'=>'db_keanggotaanorg a',
			// 	'join'=>array(array('tabel'=>'db_pegawai b','ON'=>'a.idpeg::varchar=b.idpeg::varchar','jenis'=>'RIGHT')),
			// 	'where'=>array(array('b.kategori'=>'pendidik')),
			// 	'groupby'=>array('b.nama','b.idpeg','b.gelardepan','b.gelarbelakang'),
			// 	'order'=>array('kolom'=>'jumlah','orderby'=>'DESC'),				
			// );
			$hardcode="SELECT b.idpeg,b.gelardepan,b.nama,b.gelarbelakang,count(a.id) AS jumlah FROM db_keanggotaanorg a
						RIGHT JOIN db_pegawai b ON b.idpeg::VARCHAR=a.idpeg WHERE b.kategori='pendidik' GROUP BY b.nama,b.gelardepan,b.idpeg,b.gelarbelakang ORDER BY jumlah DESC";
			$data=array(
				'menu'=>'tridharma',
				'submenu'=>'organisasi',
				'headline'=>'organisasi',
				'judul'=>'laporan organisasi profesi',
				// 'data'=>$this->Crud->join($query)->result(),
				'data'=>$this->Crud->hardcode($hardcode)->result(),
			);			
			//print_r($data);		
			$this->load->view('administrator',$data);		
		}										
	}
?>