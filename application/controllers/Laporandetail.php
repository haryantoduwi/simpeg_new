<?php
	Class Laporandetail extends CI_Controller{
		function __construct(){
			parent::__construct();
			$this->idpegawai=$this->session->userdata('id_pegawai');
			$this->load->model(array('mseminarpenelitian','mtahunakad','Crud'));
			if($this->session->userdata('login')!=true){
				redirect(site_url('Login/logout'));
			}				
		}
		//DEKLARASI VAR
		//private $master_tabel='serdos';
		private $msg_simpansuccess="Data berhasil disimpan";
		private $msg_hapussuccess="Data berhasil dihapus";
		private $default_url='Laporan';
		private $default_view="Laporandetail/";

		private function get_nama($id){
			$query=array(
				'select'=>'a.*',
				'tabel'=>'db_pegawai a',
				'where'=>array(array('a.idpeg'=>$id)),
			);
			$data=array(
				'nama'=>$this->Crud->read($query)->row(),
			);
			return $data;
		}
		function fileupload($path,$file){
			$config=array(
				'upload_path'=>$path,
				'allowed_types'=>'pdf',
				'max_size'=>5000, //5mb
				'encrypt_name'=>true,
			);
			$this->load->library('upload',$config);
			return $this->upload->do_upload($file);
		}			
		function index(){		
			redirect(site_url($this->default_url.'/detailserkom'));
		}
		public function detailserkom($file=null){
			if(!$file){
				$id=$this->input->post('id');
				$query=array(
					'select'=>'b.idpeg,b.gelardepan,b.nama,b.gelarbelakang,a.*',
					'tabel'=>'serkom a',
					'join'=>array(array('tabel'=>'db_pegawai b','ON'=>'a.serkom_idpegawai=b.idpeg','jenis'=>'INNER')),
					'where'=>array(array('b.idpeg'=>$id)),	
					'order'=>array('kolom'=>'serkom_tahunakademik','orderby'=>'DESC'),		
				);								
				$data=array(
					'menu'=>'laporan',
					'submenu'=>'laporanserkom',
					'headline'=>'laporanserkom',
					'judul'=>'Detail sertifikat kompetensi',
					'data'=>$this->Crud->join($query)->result(),
				);
				$this->load->view($this->default_view.'serkom',$data);				
				//print_r($data['tendik']);		
			}else{
				$path='./berkas/fileserkom/';
				$link=$path.$file;
				if(file_exists($link)){
					$url=file_get_contents($link);
					force_download($file,$url);
				}else{
					$this->session->set_flashdata('error','File tidak ditemukan');
					redirect(site_url('Laporan/serkom'));
				}				
			}	
		}
		public function detailserdos($file=null){
			if(!$file){
				$id=$this->input->post('id');
				$query=array(
					'select'=>'b.idpeg,b.gelardepan,b.nama,b.gelarbelakang,a.*',
					'tabel'=>'serdos a',
					'join'=>array(array('tabel'=>'db_pegawai b','ON'=>'a.serdos_idpegawai=b.idpeg','jenis'=>'INNER')),
					'where'=>array(array('b.idpeg'=>$id)),			
				);								
				$data=array(
					'judul'=>'Detail sertifikat dosen',
					'data'=>$this->Crud->join($query)->result(),
				);
				$this->load->view($this->default_view.'serdos',$data);				
				//print_r($data['tendik']);		
			}else{
				$path='./berkas/fileserdos/';
				$link=$path.$file;
				if(file_exists($link)){
					$url=file_get_contents($link);
					force_download($file,$url);
				}else{
					$this->session->set_flashdata('error','File tidak ditemukan');
					redirect(site_url('Laporan/serdos'));
				}				
			}	
		}	
		public function detailseminartendik($file=null){
			if(!$file){
				$id=$this->input->post('id');
				$query=array(
					'select'=>'b.idpeg,b.gelardepan,b.nama,b.gelarbelakang,a.*',
					'tabel'=>'db_seminartendik a',
					'join'=>array(array('tabel'=>'db_pegawai b','ON'=>'a.seminar_idpeg=b.idpeg','jenis'=>'INNER')),
					'where'=>array(array('b.idpeg'=>$id)),			
				);								
				$data=array(
					'judul'=>'Detail sertifikat pegawai tendik',
					'data'=>$this->Crud->join($query)->result(),
				);
				$this->load->view($this->default_view.'seminartendik',$data);				
				//print_r($data['tendik']);		
			}else{
				$path='./berkas/seminartendik/';
				$link=$path.$file;
				if(file_exists($link)){
					$url=file_get_contents($link);
					force_download($file,$url);
				}else{
					$this->session->set_flashdata('error','File tidak ditemukan');
					redirect(site_url('Laporan/seminartendik'));
				}				
			}	
		}
		public function detailrekognisi($file=null){
			if(!$file){
				$id=$this->input->post('id');
				$query=array(
					'select'=>'b.idpeg,b.gelardepan,b.nama,b.gelarbelakang,a.*',
					'tabel'=>'db_rekognisi a',
					'join'=>array(array('tabel'=>'db_pegawai b','ON'=>'a.rekognisi_idpeg=b.idpeg','jenis'=>'INNER')),
					'where'=>array(array('b.idpeg'=>$id)),			
				);								
				$data=array(
					'judul'=>'Detail rekognisi dosen',
					'data'=>$this->Crud->join($query)->result(),
				);
				$this->load->view($this->default_view.'rekognisi',$data);				
				//print_r($data['tendik']);		
			}else{
				$path='./berkas/rekognisi/';
				$link=$path.$file;
				if(file_exists($link)){
					$url=file_get_contents($link);
					force_download($file,$url);
				}else{
					$this->session->set_flashdata('error','File tidak ditemukan');
					redirect(site_url('Laporan/rekognisi'));
				}				
			}	
		}
		public function detailpenelitian($file=null){
			if(!$file){
				$id=$this->input->post('id');
				$pegawai=$this->get_nama($id);
				$query=array(
					'select'=>'b.idpeg as idpegawai,b.gelardepan,b.nama,b.gelarbelakang,a.*',
					'tabel'=>'db_penelitian a',
					'join'=>array(array('tabel'=>'db_pegawai b','ON'=>'a.idpeg=b.idpeg','jenis'=>'INNER')),
					'where'=>array(array('b.idpeg'=>$id)),		
					'order'=>array('kolom'=>'tahunakademik','orderby'=>'DESC'),	
				);								
				$data=array(
					'judul'=>'Detail penelitian dosen',
					'nama'=>$pegawai['nama'],
					'data'=>$this->Crud->join($query)->result(),
				);
				$this->load->view($this->default_view.'penelitian',$data);				
				//print_r($data['tendik']);		
			}else{
				$path='./berkas/penelitian/';
				$link=$path.$file;
				if(file_exists($link)){
					$url=file_get_contents($link);
					force_download($file,$url);
				}else{
					$this->session->set_flashdata('error','File tidak ditemukan');
					redirect(site_url('Laporan/penelitian'));
				}				
			}	
		}
		public function detailjurnal($file=null){
			if(!$file){
				$id=$this->input->post('id');
				$query=array(
					'select'=>'b.idpeg as idpegawai,b.gelardepan,b.nama,b.gelarbelakang,a.*',
					'tabel'=>'db_jurnal a',
					'join'=>array(array('tabel'=>'db_pegawai b','ON'=>'a.idpeg=b.idpeg','jenis'=>'INNER')),
					'where'=>array(array('b.idpeg'=>$id)),
					'order'=>array('kolom'=>'thakademik','orderby'=>'DESC')			
				);								
				$data=array(
					'judul'=>'Detail penulisan jurnal dosen',
					'data'=>$this->Crud->join($query)->result(),
				);
				$this->load->view($this->default_view.'jurnal',$data);				
				//print_r($data['tendik']);		
			}else{
				//PATH DOWNLOAD BERKAS, SESUAIKAN DENGAN PATH YANG ADA
				$path='./berkas/jurnal/';
				/////////////////////////////////////////////////////
				$link=$path.$file;
				if(file_exists($link)){
					$url=file_get_contents($link);
					force_download($file,$url);
				}else{
					$this->session->set_flashdata('error','File tidak ditemukan');
					redirect(site_url('Laporan/jurnal'));
				}				
			}	
		}									
		public function detailabdimas($file=null){
			if(!$file){
				$id=$this->input->post('id');
				$query=array(
					'select'=>'b.idpeg as idpegawai,b.gelardepan,b.nama,b.gelarbelakang,a.*',
					'tabel'=>'db_abdimas a',
					'join'=>array(array('tabel'=>'db_pegawai b','ON'=>'a.idpeg=b.idpeg','jenis'=>'INNER')),
					'where'=>array(array('b.idpeg'=>$id)),	
					'order'=>array('kolom'=>'thnakademik','orderby'=>'DESC')		
				);								
				$data=array(
					'judul'=>'Detail abdimas dosen',
					'data'=>$this->Crud->join($query)->result(),
				);
				$this->load->view($this->default_view.'abdimas',$data);				
				//print_r($data['tendik']);		
			}else{
				//PATH DOWNLOAD BERKAS, SESUAIKAN DENGAN PATH YANG ADA
				$path='./berkas/abdimas/';
				/////////////////////////////////////////////////////
				$link=$path.$file;
				if(file_exists($link)){
					$url=file_get_contents($link);
					force_download($file,$url);
				}else{
					$this->session->set_flashdata('error','File tidak ditemukan');
					redirect(site_url('Laporan/abdimas'));
				}				
			}	
		}
		public function detailseminar($file=null){
			if(!$file){
				$id=$this->input->post('id');
				$query=array(
					'select'=>'b.idpeg as idpegawai,b.gelardepan,b.nama,b.gelarbelakang,a.*',
					'tabel'=>'db_seminar a',
					'join'=>array(array('tabel'=>'db_pegawai b','ON'=>'a.idpeg=b.idpeg','jenis'=>'INNER')),
					'where'=>array(array('b.idpeg'=>$id)),
					'order'=>array('kolom'=>'tahunakademik','orderby'=>'DESC')

				);								
				$data=array(
					'judul'=>'Detail seminar dosen',
					'data'=>$this->Crud->join($query)->result(),
				);
				$this->load->view($this->default_view.'seminar',$data);				
				//print_r($data['tendik']);		
			}else{
				//PATH DOWNLOAD BERKAS, SESUAIKAN DENGAN PATH YANG ADA
				$path='./berkas/seminar/';
				/////////////////////////////////////////////////////
				$link=$path.$file;
				if(file_exists($link)){
					$url=file_get_contents($link);
					force_download($file,$url);
				}else{
					$this->session->set_flashdata('error','File tidak ditemukan');
					redirect(site_url('Laporan/seminar'));
				}				
			}	
		}
		public function detailhaki($file=null){
			if(!$file){
				$id=$this->input->post('id');
				$pegawai=$this->get_nama($id);
				$query=array(
					'select'=>'b.idpeg as idpegawai,b.gelardepan,b.nama,b.gelarbelakang,a.*',
					'tabel'=>'db_haki a',
					'join'=>array(array('tabel'=>'db_pegawai b','ON'=>'a.idpeg=b.idpeg','jenis'=>'INNER')),
					'where'=>array(array('b.idpeg'=>$id)),			
				);								
				$data=array(
					'judul'=>'Detail kepemiliman HAKI dosen',
					'nama'=>$pegawai['nama'],
					'data'=>$this->Crud->join($query)->result(),
				);
				$this->load->view($this->default_view.'haki',$data);				
				//print_r($data['tendik']);		
			}else{
				//PATH DOWNLOAD BERKAS, SESUAIKAN DENGAN PATH YANG ADA
				$path='./berkas/haki/';
				/////////////////////////////////////////////////////
				$link=$path.$file;
				if(file_exists($link)){
					$url=file_get_contents($link);
					force_download($file,$url);
				}else{
					$this->session->set_flashdata('error','File tidak ditemukan');
					redirect(site_url('Laporan/haki'));
				}				
			}	
		}			
		public function detailpenulisanbuku($file=null){
			if(!$file){
				$id=$this->input->post('id');
				$pegawai=$this->get_nama($id);
				$query=array(
					'select'=>'b.idpeg as idpegawai,b.gelardepan,b.nama,b.gelarbelakang,a.*',
					'tabel'=>'db_buku a',
					'join'=>array(array('tabel'=>'db_pegawai b','ON'=>'a.idpeg=b.idpeg','jenis'=>'INNER')),
					'where'=>array(array('b.idpeg'=>$id)),			
				);								
				$data=array(
					'judul'=>'Detail penulisan buku dosen',
					'nama'=>$pegawai['nama'],
					'data'=>$this->Crud->join($query)->result(),
				);
				$this->load->view($this->default_view.'buku',$data);				
				//print_r($data['tendik']);		
			}else{
				//PATH DOWNLOAD BERKAS, SESUAIKAN DENGAN PATH YANG ADA
				$path='./berkas/penulisanbuku/';
				/////////////////////////////////////////////////////
				$link=$path.$file;
				if(file_exists($link)){
					$url=file_get_contents($link);
					force_download($file,$url);
				}else{
					$this->session->set_flashdata('error','File tidak ditemukan');
					redirect(site_url('Laporan/buku'));
				}				
			}	
		}
		public function detailkeorganisasian($file=null){
			if(!$file){
				$id=$this->input->post('id');
				$pegawai=$this->get_nama($id);
				$query=array(
					'select'=>'b.idpeg as idpegawai,b.gelardepan,b.nama,b.gelarbelakang,a.*',
					'tabel'=>'db_keangotaanorg a',
					'join'=>array(array('tabel'=>'db_pegawai b','ON'=>'a.idpeg::varchar=b.idpeg','jenis'=>'INNER')),
					'where'=>array(array('b.idpeg'=>$id)),			
				);
				$hardcode="SELECT b.idpeg,b.gelardepan,b.nama,b.gelarbelakang,a.* FROM db_keanggotaanorg a
						RIGHT JOIN db_pegawai b ON b.idpeg::VARCHAR=a.idpeg WHERE b.idpeg=$id";

				$data=array(
					'judul'=>'Detail keanggotaan organisasi dosen',
					// 'data'=>$this->Crud->join($query)->result(),
					'nama'=>$pegawai['nama'],
					'data'=>$this->Crud->hardcode($hardcode)->result(),
				);
				$this->load->view($this->default_view.'keanggotaan',$data);				
				//print_r($data['tendik']);		
			}else{
				//PATH DOWNLOAD BERKAS, SESUAIKAN DENGAN PATH YANG ADA
				$path='./berkas/keangotaan/';
				/////////////////////////////////////////////////////
				$link=$path.$file;
				if(file_exists($link)){
					$url=file_get_contents($link);
					force_download($file,$url);
				}else{
					$this->session->set_flashdata('error','File tidak ditemukan');
					redirect(site_url('Laporan/organisasi'));
				}				
			}	
		}	
		public function detailjabatanfungsional($file=null){
			if(!$file){
				$id=$this->input->post('id');
				$pegawai=$this->get_nama($id);
				$query=array(
					'select'=>'b.idpeg as idpegawai,b.gelardepan,b.nama,b.gelarbelakang,a.*',
					'tabel'=>'db_jafa a',
					'join'=>array(array('tabel'=>'db_pegawai b','ON'=>'a.idpeg=b.idpeg','jenis'=>'INNER')),
					'where'=>array(array('b.idpeg'=>$id)),	
					'order'=>array('kolom'=>'tmt','orderby'=>'DESC')		
				);
				$hardcode="SELECT b.idpeg,b.gelardepan,b.nama,b.gelarbelakang,a.* FROM db_keanggotaanorg a
					RIGHT JOIN db_pegawai b ON b.idpeg::VARCHAR=a.idpeg WHERE b.idpeg=$id";
				$data=array(
					'judul'=>'Detail Laporan Jabatan Fungsional',
					// 'data'=>$this->Crud->join($query)->result(),
					'nama'=>$pegawai['nama'],
					'data'=>$this->Crud->join($query)->result(),
				);
				$this->load->view($this->default_view.'jabatanfungsional',$data);				
				//print_r($data['tendik']);		
			}else{
				//PATH DOWNLOAD BERKAS, SESUAIKAN DENGAN PATH YANG ADA
				$path='./berkas/jafa/';
				/////////////////////////////////////////////////////
				$link=$path.$file;
				if(file_exists($link)){
					$url=file_get_contents($link);
					force_download($file,$url);
				}else{
					$this->session->set_flashdata('error','File tidak ditemukan');
					redirect(site_url('Laporan/jabatanfungsional'));
				}				
			}	
		}
		public function detailjabatanprofesi($file=null){
			if(!$file){
				$id=$this->input->post('id');
				$pegawai=$this->get_nama($id);
				$query=array(
					'select'=>'b.idpeg as idpegawai,b.gelardepan,b.nama,b.gelarbelakang,a.*',
					'tabel'=>'db_riwayatjabatan a',
					'join'=>array(array('tabel'=>'db_pegawai b','ON'=>'a.idpeg=b.idpeg','jenis'=>'INNER')),
					'where'=>array(array('b.idpeg'=>$id)),	
					'order'=>array('kolom'=>'tmt','orderby'=>'DESC')		
				);
				$hardcode="SELECT b.idpeg,b.gelardepan,b.nama,b.gelarbelakang,a.* FROM db_keanggotaanorg a
					RIGHT JOIN db_pegawai b ON b.idpeg::VARCHAR=a.idpeg WHERE b.idpeg=$id";
				$data=array(
					'judul'=>'Detail Laporan Riawayat Jabatan',
					// 'data'=>$this->Crud->join($query)->result(),
					'nama'=>$pegawai['nama'],
					'data'=>$this->Crud->join($query)->result(),
				);
				$this->load->view($this->default_view.'jabatanprofesi',$data);				
				//print_r($data['tendik']);		
			}else{
				//PATH DOWNLOAD BERKAS, SESUAIKAN DENGAN PATH YANG ADA
				$path='./berkas/jabatan/';
				/////////////////////////////////////////////////////
				$link=$path.$file;
				if(file_exists($link)){
					$url=file_get_contents($link);
					force_download($file,$url);
				}else{
					$this->session->set_flashdata('error','File tidak ditemukan');
					redirect(site_url('Laporan/jabatanprofesi'));
				}				
			}	
		}																		
	}
?>