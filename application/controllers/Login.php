<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Login extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -  
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in 
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see http://codeigniter.com/user_guide/general/urls.html
	 */
	function __construct(){
		parent::__construct();
		$this->load->model(array('mlogin'));
	}
	// function buat_captcha(){
	// 	$config=array(
	// 		'img_path'=>'./captcha/',
	// 		'img_url'=>base_url().'/captcha/',
	// 		'img_width'=>'150',
	// 		'img_height'=>'30',
	// 		'word_length'=> 8,
	// 		'expiration'=>7200,
	// 		);
	// 	$cap=create_captcha($config);
	// 	$image=$cap['image'];
	// 	$this->session->set_userdata('captchaword',$cap['word']);
	// 	return $image;
	// }
	function cek_captcha(){
		if($this->input->post('captcha')==$this->session->userdata('captchaword')){
			return true;
		}else{
			$this->form_validation->set_message('cek_captcha','Captcha tidak cocok');
			return false;
		}
	}
	public function index()
	{
		$this->form_validation->set_rules('username','Username','required');
		$this->form_validation->set_rules('password','Password','required');
		if($this->form_validation->run()==true){
			$username=$this->input->post('username');
			$password=$this->input->post('password');
			$res=$this->mlogin->cek_user($username,$password);
			//CEK USER BIASA APA ADMIN
			if($res->num_rows()==1){
				$datauser=$res->row();
				$data_session=array(
					'login'=>true,
					'nama'=>$datauser->nama,
					'id_pegawai'=>$datauser->idpeg,
					'foto'=>$datauser->foto,
					'email'=>$datauser->email,	
					);
				$this->session->set_userdata($data_session);
				redirect(site_url('User'));				
			}elseif($res->num_rows()>=1){
				$this->session->set_flashdata("error","Kontak Administrator, Username anda ke suspend");
				$this->login();				
			}else{
				$query=$this->mlogin->cek_admin($username,$password);
				if($query->num_rows()==1){
					$dataadmin=$query->row();
					$data_session=array(
						'login'=>true,
						'username'=>$dataadmin->username,
						'password'=>$dataadmin->password,
						'email'=>"p3si@akprind.ac.id",
						'foto'=>"user.jpg",
						'level'=>1,
					);
					$this->session->set_userdata($data_session);
					redirect(site_url('Admin'));
				}elseif($query->num_rows()>=1){
					$this->session->set_flashdata("error","Kontak Administrator, Username anda ke suspend");
					$this->login();
				}else{
					$this->session->set_flashdata("error","Username dan Password tidak terdaftar");
					$this->login();
				}
			}
		}else{
			$this->load->view('coreUI/bodylogin');
		}
	}
	public function login_as($username,$password){
			$res=$this->mlogin->cek_user($username,$password);
			//CEK USER BIASA APA ADMIN
			if($res->num_rows()==1){
				$datauser=$res->row();
				$data_session=array(
					'login'=>true,
					'nama'=>$datauser->nama,
					'id_pegawai'=>$datauser->idpeg,
					'foto'=>$datauser->foto,
					'fromadmin'=>true,
					'email'=>$datauser->email,	
					);
				$this->session->set_userdata($data_session);
				redirect(site_url('User'));				
			}elseif($res->num_rows()>=1){
				$this->session->set_flashdata("error","Kontak Administrator, Username anda ke suspend");
				$this->login();
			}		
	}
	public function fromadmin($param){
				$username=$this->session->userdata('username');
				$password=$this->session->userdata('password');				
				$query=$this->mlogin->cek_admin($username,$password);
				if($query->num_rows()==1){
					$dataadmin=$query->row();
					$data_session=array(
						'login'=>true,
						'username'=>$dataadmin->username,
						'password'=>$dataadmin->password,
						'email'=>"p3si@akprind.ac.id",
						'foto'=>"user.jpg",
					);
					$this->session->set_userdata($data_session);
					if($param){
						redirect(site_url('Admin/pegawai'));
					}else{
						//echo $param;
						redirect(site_url('Admin'));
					}					
					//redirect(site_url('Admin'));
				}elseif($query->num_rows()>=1){
					$this->session->set_flashdata("error","Kontak Administrator, Username anda ke suspend");
					$this->login();
				}else{
					$this->session->set_flashdata("error","Username dan Password tidak terdaftar");
					$this->login();
				}		
	}
	public function login(){
		$this->load->view('coreUI/bodylogin');
	}
	public function logout(){
		$this->session->sess_destroy();
		$this->session->set_flashdata("success","Anda berhasil Logout");
		redirect(site_url('Login'));
	}
}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */