<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Mutasi extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -  
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in 
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see http://codeigniter.com/user_guide/general/urls.html
	 */
	function __construct(){
		parent:: __construct();
		$this->load->model(array('mmutasi'));
		if(!$this->session->userdata('login')){
			redirect(site_url('Login'));
		}
	}

	//DEKLARASI VAR
	var $msg_simpansuccess="Data berhasil disimpan";
	var $msg_hapussuccess="Data berhasil dihapus";		
	
	public function index()
	{
		redirect(site_url('Mutasi/simpan_data'));
	}
	public function simpan_data(){
		$idpeg=$this->session->userdata('id_pegawai');
		$data=array(
			'idpeg'=>$idpeg,
			'tglmutasi'=>$this->input->post('tgl_ditetapkan'),
			'sampai'=>$this->input->post('sampai'),
			'unitasal'=>$this->input->post('unitkerja_asal'),
			'unitbaru'=>$this->input->post('unitkerja_baru')
			);
		//print_r($data);
		$query=$this->mmutasi->simpan_mutasi($data);
		if($query){
			$this->session->set_flashdata('success',$this->msg_simpansuccess);
		}else{
			$error=$this->db->error();
			$this->session->set_flashdata('error',$error['message']);
		}
		redirect(site_url('User/mutasi'));
	}
	public function hapus_data($id){
		$query=$this->mmutasi->hapus_mutasi($id);
		if($query){
			$this->session->set_flashdata('success',$this->msg_hapussuccess);
		}else{
			$error=$this->db->error();
			$this->session->set_flashdata('error',$error['message']);
		}
		redirect(site_url('User/mutasi'));				
	}
}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */