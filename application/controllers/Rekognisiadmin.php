<?php
	Class Rekognisiadmin extends CI_Controller{
		function __construct(){
			parent::__construct();
			$this->idpegawai=$this->session->userdata('id_pegawai');
			$this->load->model(array('mseminarpenelitian','mtahunakad','Crud'));
			if($this->session->userdata('login')!=true){
				redirect(site_url('Login/logout'));
			}			
		}
		//DEKLARASI VAR
		private $master_tabel='db_rekognisi';
		private $id_tabel='rekognisi_id';
		private $msg_Updatesuccess="Data berhasil diUpdate";
		private $msg_hapussuccess="Data berhasil dihapus";
		private $default_url='Rekognisiadmin';
		private $default_view='rekognisiadmin/';
		//private $path="./berkas/seminar/";
		//DUMMY
		private $path="./berkas/rekognisi/";
		//private $path="http://php4.akprind.ac.id/simpeg/FileSeminar/";

		function fileupload($path,$file){
			$config=array(
				'upload_path'=>$path,
				'allowed_types'=>'pdf',
				'max_size'=>5000, //5mb
				'encrypt_name'=>true,
			);
			$this->load->library('upload',$config);
			return $this->upload->do_upload($file);
		}			
		function index(){
			$this->form_validation->set_rules('rekognisi_idpegawai','Id Pegawai','required');
			if($this->form_validation->run()==true){
				//$path=$this->path;$file='fileupload';
				$data=array(
					'rekognisi_idpeg'=>$this->input->post('rekognisi_idpegawai'),
					'rekognisi_tahunakademik'=>$this->input->post('rekognisi_tahunakademik'),
					'rekognisi_rekognisi'=>$this->input->post('rekognisi_rekognisi'),
					'rekognisi_tempat'=>$this->input->post('rekognisi_tempat'),
					'rekognisi_tglkegiatan'=>date('Y-m-d',strtotime($this->input->post('rekognisi_tglkegiatan'))),
				);				
				$file='fileupload';		
				if($_FILES[$file]['name']){
					if($this->fileupload($this->path,$file)){
						$uploaddata=$this->upload->data();
						$data['rekognisi_file']=$uploaddata['file_name'];
					}else{
						$dt['error']=$this->upload->display_errors();
						$this->session->set_flashdata('error',$dt['error']);
						redirect(site_url($this->default_url));
					}
				}	
				$query=array(
					'data'=>$data,
					'tabel'=>$this->master_tabel,
				);				
				$insert=$this->Crud->insert($query);
				if($insert){
					$this->session->set_flashdata('success','Update berhasil');
				}else{
					$msg='Update gagal, msg :'.$insert;
					$this->session->set_flashdata('error','Update berhasil');
				}
				redirect(site_url($this->default_url));
				//print_r($data);				
			}else{
				$query=array(
					'select'=>'a.*,b.nama,b.gelardepan,b.gelarbelakang',
					'tabel'=>'db_rekognisi a',
					'join'=>array(array('tabel'=>'db_pegawai b','ON'=>'a.rekognisi_idpeg=b.idpeg','jenis'=>'INNER')),
					'orderby'=>array('kolom'=>'b.nama','orderby'=>'ASC'),				
				);
				$pegawai=array(
					'tabel'=>'db_pegawai',
					'where'=>array(array('kategori'=>'pendidik')),
					'orderby'=>array('kolom'=>'nama','orderby'=>'ASC'),
				);
				//$var=$this->variabel();
				$data=array(
					'menu'=>'datapenunjang',
					'submenu'=>'rekognisi',
					'headline'=>'rekognisi',
					'judul'=>'daftar rekognisi',
					'pegawai'=>$this->Crud->read($pegawai)->result(),
					'data'=>$this->Crud->join($query)->result(),
					'thnakademik'=>$this->mtahunakad->get_tahunakademik()->result(),
				);
				//print_r($data['data']);
				$this->load->view('administrator',$data);				
			}
		}
		private function get_file($id){
			$query=array(
				'tabel'=>$this->master_tabel,
				'where'=>array(array('rekognisi_id'=>$id)),
			);
			$read=$this->Crud->read($query)->row();
			if($read->rekognisi_file){
				unlink($this->path.$read->rekognisi_file);
			}
		}
		function hapus($id){
			$this->get_file($id);
			$query=array(
				'tabel'=>$this->master_tabel,
				'where'=>array('rekognisi_id'=>$id),
			);
			$delete=$this->Crud->delete($query);
			if($delete){
				$this->session->set_flashdata('success','Hapus berhasil');
			}else{
				$mssg='Hapus error, msg : '.$delete;
				$this->session->set_flashdata('error',$msg);
			}
			redirect(site_url($this->default_url));		
			//print_r($query);
		}
		function edit(){
			$id=$this->input->post('id');
			$query=array(
				'tabel'=>$this->master_tabel,
				'where'=>array(array('rekognisi_id'=>$id)),
			);
			$pegawai=array(
				'tabel'=>'db_pegawai',
				'orderby'=>array('kolom'=>'nama','orderby'=>'ASC'),
			);			
			$data=array(
				'thnakademik'=>$this->mtahunakad->get_tahunakademik()->result(),
				'pegawai'=>$this->Crud->read($pegawai)->result(),
				'data'=>$this->Crud->read($query)->row(),
			);
			//print_r($data);
			$this->load->view('rekognisiadmin/form_edit',$data);			
		}
		function update(){
			$id=$this->input->post('id');
			$this->form_validation->set_rules('rekognisi_idpegawai','Id Pegawai','required');
			if($this->form_validation->run()==true){
				//$path=$this->path;$file='fileupload';
				$data=array(
					'rekognisi_idpeg'=>$this->input->post('rekognisi_idpegawai'),
					'rekognisi_tahunakademik'=>$this->input->post('rekognisi_tahunakademik'),
					'rekognisi_rekognisi'=>$this->input->post('rekognisi_rekognisi'),
					'rekognisi_tempat'=>$this->input->post('rekognisi_tempat'),
					'rekognisi_tglkegiatan'=>date('Y-m-d',strtotime($this->input->post('rekognisi_tglkegiatan'))),
				);				
				$file='fileupload';		
				if($_FILES[$file]['name']){
					if($this->fileupload($this->path,$file)){
						$uploaddata=$this->upload->data();
						$data['rekognisi_file']=$uploaddata['file_name'];
					}else{
						$dt['error']=$this->upload->display_errors();
						$this->session->set_flashdata('error',$dt['error']);
						redirect(site_url($this->default_url));
					}
				}	
				$query=array(
					'data'=>$data,
					'tabel'=>$this->master_tabel,
					'where'=>array('rekognisi_id'=>$id),
				);				
				$insert=$this->Crud->update($query);
				if($insert){
					$this->session->set_flashdata('success','Update berhasil');
				}else{
					$msg='Update gagal, msg :'.$insert;
					$this->session->set_flashdata('error','Update berhasil');
				}
				redirect(site_url($this->default_url));
				//print_r($data);
			}		
		}
		// function detail(){
		// 	$id=$this->input->post('id');
		// 	$query=array(
		// 		'where'=>array('id'=>$id),
		// 		'tabel'=>'db_seminar',
		// 	);
		// 	$data=array(
		// 		'data'=>$this->Crud->read($query)->row(),
		// 		);
		// 	$this->load->view('seminar/detail',$data);
		// }
		function downloadfile($file){
			$file=str_replace('%20',' ', $file);
			$link=$this->path.$file;
			$url=file_get_contents($link);
			$download=force_download($file,$url);
			if(!$download){
				$this->session->set_flashdata('error','File tidak ditemukan,silahkan upload ulang');
				redirect(site_url('User/seminarpemakalah'));	
			}			
			// if(file_exists($link)){
			// 	$url=file_get_contents($link);
			// 	force_download($file,$url);
			// }else{
			// 	$this->session->set_flashdata('error','File tidak ditemukan');
			// 	redirect(site_url("User/seminarpemakalah"));	
			// }			
		}		
	}
?>