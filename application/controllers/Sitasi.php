<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Sitasi extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -  
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in 
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see http://codeigniter.com/user_guide/general/urls.html
	 */
	function __construct(){
		parent:: __construct();
		$this->load->model(array('Crud'));
		if(!$this->session->userdata('login')){
			redirect(site_url('Login'));
		}
	}

	//DEKLARASI VAR		
	public function notifikasi($param){
		if($param==1){
			$this->session->set_flashdata('success','Proses berhasil');
		}else{
			$this->session->set_flashdata('error',$param);
		}
	}
	public function index()
	{
		$idpeg=$this->session->userdata('id_pegawai');
		$data=array(
			'sitasi_idpegawai'=>$this->input->post('sitasi_idpegawai'),
			'sitasi_kodekategori'=>$this->input->post('sitasi_kodekategori'),
			'sitasi_url'=>'http://'.$this->input->post('sitasi_url'),
		);
		//print_r($data);
		$query=array(
			'tabel'=>'db_sitasi',
			'data'=>$data,
		);
		$insert=$this->Crud->insert($query);
		$this->notifikasi($insert);
		redirect(site_url('User/sitasi'));
	}
	public function hapus($id){
		$query=array(
			'tabel'=>'db_sitasi',
			'where'=>array('sitasi_id'=>$id),
		);	
		$delete=$this->Crud->delete($query);
		$this->notifikasi($delete);
		redirect(site_url('User/sitasi'));				
	}
	public function edit(){
		$id=$this->input->post('id');
		if($this->input->post('submit')){
			$data=array(
				'sitasi_kodekategori'=>$this->input->post('sitasi_kodekategori'),
				'sitasi_url'=>'http://'.$this->input->post('sitasi_url'),
			);	
			$query=array(
				'tabel'=>'db_sitasi',
				'data'=>$data,
				'where'=>array('sitasi_id'=>$id),
			);	
			$update=$this->Crud->update($query);
			$this->notifikasi($update);
			redirect(site_url('User/sitasi'));	
		}else{
			$query=array(
				'tabel'=>'db_sitasi',
				'where'=>array('sitasi_id'=>$id),
			);
			$kategori=array(
				'tabel'=>'db_kategorisitasi',
			);		
			$data=array(
				'data'=>$this->Crud->read($query)->row(),
				'kategori'=>$this->Crud->read($kategori)->result()
			);
			$this->load->view('sitasi/edit',$data);			
		}
	}
}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */