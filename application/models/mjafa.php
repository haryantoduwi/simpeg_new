<?php
	Class mjafa extends CI_Model{
		function __construct(){
			parent::__construct();
		}
		function get_jafa_byidpeg($id){
			$this->db->where('idpeg',$id);
			return $this->db->get('db_jafa');
		}
		function simpan_jafa($data){
			if($this->db->insert('db_jafa',$data)){
				return true;	
			}else{
				return false;
			}
		}
		function hapus_jafa($id){
			$this->db->where('id',$id);
			if($this->db->delete('db_jafa')){
				return true;
			}else{
				return false;
			}
		}

	}
?>