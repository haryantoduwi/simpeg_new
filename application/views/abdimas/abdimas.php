<section class="content">
	<div class="container-fluid">
        <!--ALERT-->
		<?php
			if($this->session->flashdata('error')==true){
				echo '
		        <div class="alert bg-red alert-dismissible">
		        	<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
		            <b>Perhatian !</b> '.$this->session->flashdata('error').'
		        </div>
				';
			}elseif($this->session->flashdata('success')==true){
				echo '
		        <div class="alert bg-green alert-dismissible">
		        	<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
		            <b>Perhatian !</b> '.$this->session->flashdata('success').'
		        </div>
				';				
			}
		?>	
			
		<div class="block-header">
			<h2>ABDIMAS</h2>
		</div>
		<!--FORM TAMBAH DATA-->
		<div class="row clearfix formtambah">
			<div class="col-sm-3">
				<div class="form-group">
					<button type="button" class="tomboltambah btn btn-primary btn-lg m-t-15 waves-effect btn-block">Tambah</button>
				</div>
			</div>
		</div>		
		<!--TAMBAH DATA-->
		<div class="row clearfix">
			<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
				<div class="card forminput"  style="display:none">
					<div class="body">
						<form id="formabdimas" enctype="multipart/form-data" action="<?php echo site_url('Abdimas')?>" method="POST" >
							<h2 class="card-inside-title">Daftar Abdimas</h2>
							<div class="row clearfix">
								<div class="col-sm-12">
									<div class="form-group form-float hide">
										<div class="form-line">
											<input type="text" readonly class="form-control " name="idpeg" value="<?php echo $this->session->userdata('id_pegawai')?>">
											<label class="form-label">Id Pegawai</label>
										</div>
									</div>								
									<div class="form-group">
	                                    <p>
	                                        <b>Tahun Akademik</b>
	                                    </p>
	                                    <select required name="thnakademik" class="form-control show-tick" data-size="5" data-live-search="true" title="Pilih Tahun Akademik">
	                                    	<?php
	                                    		foreach ($thnakademik as $value) {
	                                    			echo "<option>".$value->tahun."</option>";
	                                    		}
	                                    	?>
	                                    </select>
									</div>
									<div class="form-group form-float">
										<div class="form-line">
											<textarea class="form-control" required name="tema"></textarea>
											<label class="form-label">Tema Penelitian</label>
										</div>
									</div>
									<div class="form-group form-float">
										<div class="form-line">
											<p><b>Biaya</b></p>
											<input type="text" name="biaya" class="form-control price"> 	
										</div>
									</div>
									<div class="form-group form-float">
										<div class="form-line">
											<p><b>Asal Dana</b></p>
											<input type="text" name="asaldana" class="form-control">
										</div>
									</div>
									<div class="form-group">
										<div class="form-line">
											<p>
												<b>Tanggal Mulai</b>
											</p>
											<input type="text" class="form-control datepicker" name="tglmulai">
										</div>
									</div>
									<div class="form-group">
										<div class="form-line">
											<p>
												<b>Tanggal Selesai</b>
											</p>										
											<input type="text" class="form-control datepicker" required name="tglselesai">
										</div>
									</div>
									<div class="form-group form-float">
										<div class="form-line">
											<textarea class="form-control" name="lokasi" required></textarea>
											<label class="form-label">Lokasi</label>
										</div>
									</div>
									<div class="form-group form-float">
										<div class="form-line">
											<textarea class="form-control" name="abstrak" rows="4" required></textarea>
											<label class="form-label">Abstrak</label>
										</div>
									</div>									
									<h2 class="card-inside-title">Upload File Pendukung</h2>
									<div class="form-group">
										<input required type="file" name="fileupload">
										<p class="help-block col-red">Ukuran Maksimal 5mb</p>
									</div>																											
								</div>
							</div>
							<div class="row clearfix">
								<div class="col-sm-12">
									<button type="submit" class="btn btn-primary btn-lg waves-effect btn-block">SIMPAN</button>									
									<button type="button" class="tutup_input btn btn-danger btn-lg waves-effect btn-block">Tutup</button>
								</div>
							</div>
						</form>
					</div>
				</div>
			</div>
		</div>
		<!--EDIT FORM-->
		<div class="row clearfix">
			<div class="col-sm-12">
				<div class="card" id="edit_card" style="display:none">
					<div class="header bg-orange">
		                <h2>
                          Edit Data
                        </h2>
                        <ul class="header-dropdown m-r--5">
                                <li class="dropdown">
                                    <a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
                                        <i class="material-icons">more_vert</i>
                                    </a>
                                    <!---->
                                    <ul class="dropdown-menu pull-right">
                                        <li><a href="javascript:void(0);" class="tutup_card">Tutup</a></li>
                                    </ul>
                                </li>
                         </ul>                        				
					</div>				
					<div class="body" id="edit_form">
						<!--AJAX LOAD HERE-->
					</div>
				</div>				
			</div>
		</div>
		<!--TABEL ABDIMAS-->
		<div class="row clearfix" id="tampildata">
			<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
				<div class="card">
					<div class="header">
		                <h2>
                           Daftar Riwayat Abdimas
                        </h2>
                        <ul class="header-dropdown m-r--5">
                                <li class="dropdown">
                                    <a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
                                        <i class="material-icons">more_vert</i>
                                    </a>
                                </li>
                         </ul>				
					</div>
					<div class="body">
						<div class="row clearfix">
							<div class="col-sm-12">
								<div class="table-responsive">
									<table class="table table-bordered table-striped table-hover tabelsimple" width="100%">
										<thead>
											<tr>
												<th width="5%">No</th>
												<th width="5%">Tahun</th>
												<th width="35%">Tema</th>
												<th width="35%">Lokasi</th>
												<th width="20%" class="text-center">Aksi</th>												
											</tr>
										</thead>
										<tbody>
											<?php
												$i=1;
												foreach ($abdimas as $val) {
													echo "<tr>";
														echo "<td>".$i."</td>";
														echo "<td>".$val->thnakademik."</td>";
														echo "<td>".$val->tema."</td>";
														echo "<td>".ucwords($val->lokasi)."</td>";
														echo "<td class='text-center'>
															<a href='#' url='".site_url('Abdimas/detail/')."' id='".$val->id."' style='width:30px' class='detail btn btn-xs btn-info waves-effect'><i class='material-icons'>label</i></a>
															";
															if(!empty($val->file)){
																echo "<a href='".base_url('Abdimas/downloadfile/'.trim($val->file))."' class='btn btn-xs btn-info waves-effect'><i class='material-icons'>file_download</i></a> ";
															}else{
																echo "<a href='#' class='btn btn-xs btn-default waves-effect'><i class='material-icons'>file_download</i></a> ";
															}
															
														echo "<a href='".site_url('Abdimas/hapus/'.$val->id)."' class='hapus btn btn-xs btn-danger waves-effect'><i class='material-icons'>delete</i></a>  
															<a href='#' id='".$val->id."' link='".site_url('Abdimas/edit_data')."' class='edit_data btn btn-xs btn-warning waves-effect'><i class='material-icons'>mode_edit</i></a> 
														</td>";
													echo "</tr>";
													$i++;
												}
											?>										
										</tbody>
									</table>
									<p>Keterangan :</p>
									<a href='#' style="width:30px; margin:2px;" class='btn btn-xs btn-info waves-effect'><i class='material-icons'>label</i></a> : Detail <br>
									<a href='#' style="width:30px; margin:2px;" class='btn btn-xs btn-info waves-effect'><i class='material-icons'>file_download</i></a> : Download File <br>
									<a href='#' style="width:30px; margin:2px" class='btn btn-xs btn-danger waves-effect'><i class='material-icons'>delete</i></a> : Tombol Hapus <br>
									<a href='#' style="width:30px;margin:2px" class='btn btn-xs btn-warning waves-effect'><i class='material-icons'>mode_edit</i></a> : Tombol Edit <br> 
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<!--END TABEL ABDIMAS-->			
	</div>
</section>
<div class="modal fade" id="detail" tabindex="-1" role="dialog">
</div>