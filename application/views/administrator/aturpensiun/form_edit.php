<form  action="<?php echo base_url('Aturpensiun/update')?>" method="POST" enctype="multipart/form-data">
	<div class="row clearfix">
		<div class="col-sm-12">
			<div class="form-group form-float hide">
				<div class="form-line">
					<input type="text"  class="form-control" readonly name="id" value="<?= $data->id?>">
					<label class="form-label">Id</label>
				</div>
			</div>	
			<div class="row clearfix">
					<div class="col-sm-12">
                        <div class="form-group form-float">
                        	<label class="form-label">Nama</label>
                        	<div class="form-line">
                        		<input type="text" name="nama" class="form-control" value="<?= $data->nama?>">
                        	</div>
                        </div>
                        <div class="form-group form-float">
                        	<label class="form-label">Tahun</label>
                        	<div class="form-line">
                        		<input type="text" name="tahun" class="form-control" value="<?=trim($data->tahun)?>">
                        	</div>
                        </div>
                        <div class="form-group form-float">
                            <label class="form-label">Jenis</label>
                            <div class="form-line">
                                <select name="jenis" class="form-control">
                                    <?php $seldosen = (trim($data->jenis)=='dosen') ? 'selected' : ''?>
                                    <?php $seltendik = (trim($data->jenis)=='tendik') ? 'selected' : ''?>
                                    <option></option>
                                    <option value="dosen" <?=$seldosen?> >Dosen</option>
                                    <option value="tendik" <?=$seltendik?> >Tendik</option>
                                </select>
                            </div>
                        </div>
					</div>
				</div>		
		</div>
	</div>
	<div class="row clearfix">
		<div class="col-sm-12">
			<button type="submit" value="serdos" name="serdos" class="btn btn-warning btn-lg waves-effect btn-block">UPDATE</button>
			<a href="<?=base_url('Aturpensiun')?>"><button type="button" class="tutup_input btn btn-danger btn-lg waves-effect btn-block">Tutup</button></a>
		</div>
	</div>							 	
</form>
<script type="text/javascript">
    $(document).ready(function(){
        $('.datepicker2').bootstrapMaterialDatePicker({
            format: "YYYY",
            //year:true,
            time:false,
            //date:true,
            //monthPicker:true
        });         
    })
</script>