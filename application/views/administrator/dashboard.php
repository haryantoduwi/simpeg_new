<section class="content">
	<div class="container-fluid">
        <!--ALERT-->
        <div class="alert bg-red">
            <b>Perhatian !</b> Sistem sedang proses upgrade, beberapa menu di nonaktifkan. Terimakasih
        </div>			
		<div class="block-header">
			<h2><?php echo $headline ?></h2>
		</div>		
		<div class="row clearfix">
            <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                <div class="info-box bg-pink hover-expand-effect">
                    <div class="icon">
                        <i class="material-icons">people </i>
                    </div>
                    <div class="content">
                        <div class="text">Total Pegawai</div>
                        <div class="number count" data-from="0" data-to="<?php echo $jumpegawai ?>" data-speed="1000" data-fresh-interval="20"></div>
                    </div>
                </div>                
           	</div>
            <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                <div class="info-box bg-cyan hover-expand-effect">
                    <div class="icon">
                        <i class="material-icons">tag_faces</i>
                    </div>
                   	<div class="content">
                       	<div class="text">Tenaga Kependidikan</div>
                        <div class="number count" data-from="0" data-to="<?php echo $jumtendik ?>" data-speed="1000" data-fresh-interval="20"></div>
                    </div>
                </div>
            </div> 
            <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                <div class="info-box bg-green hover-expand-effect">
                    <div class="icon">
                        <i class="material-icons">tag_faces</i>
                    </div>
                   	<div class="content">
                       	<div class="text">Dosen</div>
                        <div class="number count" data-from="0" data-to="<?php echo $jumpendidik?>"  data-speed="1000" data-fresh-interval="20"></div>
                    </div>
                </div>
            </div>                       			
		</div>
        <div class="row clearfix">
            <div class="col-sm-5">
                <div class="card">
                    <div class="header">
                        <h2>
                            Pegawai Pendidik <small>Jumlah Pegawai Berdasarkan Homebase</small>
                        </h2>
                    </div>
                    <div class="body">
                        <table class="table  table-striped table-hover">
                            <thead>
                                <tr>
                                    <th width="20px">No</th>
                                    <th width="200px">Homebase</th>
                                    <th>Jumlah</th>
                                </tr>
                            </thead> 
                            <tbody>
                                <?php
                                    $i=1;
                                    $total = 0;
                                    foreach ($jumpegawaibyhomebase as $val) {
                                        echo "<tr>";
                                            echo "<td>".$i."</td>";
                                            echo "<td>".$val->nama."</td>";
                                            echo "<td>".$val->jumlah."</td>";
                                        echo "</tr>";
                                        $i++;
                                        $total+=$val->jumlah;
                                    }
                                ?>
                                <tr>
                                    <td colspan="2">Jumlah</td>
                                    <td><?php echo $total ?></td>
                                </tr>
                            </tbody>                   
                        </table>
                    </div>
                </div>            
            </div>
            <div class="col-sm-4">
                <div class="card">
                    <div class="header bg-cyan">
                        <h2>
                            Jenjang Pendidikan/Dosen <small>Jumlah Pegawai Berdasarkan Jenjang Pendidikan</small>
                        </h2>
                    </div>
                    <div class="body">
                        <table class="table  table-striped table-hover">
                            <thead>
                                <tr>
                                    <th width="20px">No</th>
                                    <th width="200px">Pendidikan</th>
                                    <th>Jumlah</th>
                                </tr>
                            </thead> 
                            <tbody>
                                <?php
                                    $i=1;
                                    $total = 0;
                                    foreach ($jenjangpendidikan as $val) {
                                        echo "<tr>";
                                            echo "<td>".$i."</td>";
                                            echo "<td>";
                                            if(trim($val->jenjang)==''){echo "Belum disi";}else{echo $val->jenjang;}
                                            echo "</td>";
                                            echo "<td>".$val->jumlah."</td>";
                                        echo "</tr>";
                                        $i++;
                                        $total+=$val->jumlah;
                                    }
                                ?>
                                <tr>
                                    <td colspan="2">Jumlah</td>
                                    <td><?php echo $total ?></td>
                                </tr>
                            </tbody>                   
                        </table>
                    </div>
                </div>                 
            </div>
        </div>
		<!--SAMPAI DISINI-->
	</div>
</section>