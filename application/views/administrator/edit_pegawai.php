<section class="content">
	<div class="container-fluid">		
        <!--KONFIRMASI AKSI-->
        <?php
            if($this->session->flashdata('success')){
                echo'
                    <div class="alert alert-success alert-dismissible" role="alert">
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        '.$this->session->flashdata("success").'
                    </div>
                ';
            }elseif($this->session->flashdata('error')){
                echo'
                    <div class="alert alert-danger alert-dismissible" role="alert">
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        '.$this->session->flashdata("error").'
                    </div>
                ';              
            }   
        ?>  
        <div class="block-header">
            <h2>Edit Pegawai</h2>
        </div>                             		
		<div class="row clearfix">
            <div class="col-lg-12 col-md-12">
                <div class="card">
                    <div class="header bg-red">
                        <h2>
                            Edit Pegawai
                        </h2>
                    </div>
                    <div class="body">
                       <form id="frmeditpegawai" method="POST" action="<?php echo site_url('Admin/update_pegawai')?>" enctype="multipart/form-data">
                            <div class="row ">
                                <div class="col-sm-12">
                                    <div class="form-group">
                                        <h2 class="card-inside-title bg-blue" style="padding: 10px">Data Pegawai</h2>
                                    </div>
                                </div>
                            </div>
                            <div class="row clearfix">
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <b>Id Pegawai</b>
                                        <div class="form-line">
                                            <input readonly type="text" class="form-control" name="idpeg" value="<?php echo $pegawai->idpeg?>">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <b>No SK Masuk</b>
                                        <div class="form-line">
                                            <input type="text" class="form-control" name="noskmasuk" value="<?php echo trim($pegawai->noskmasuk)?>">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <b>No SK tetap</b>
                                        <div class="form-line">
                                            <input type="text" class="form-control" name="nosktetap" value="<?php echo trim($pegawai->nosktetap)?>">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label>Status Pegawai</label>
                                        <div class="form-line">
                                            <select class="form-control" name="statuspegawai">
                                                <option></option>
                                                <option <?= $pegawai->statuspegawai=='Aktif' ? 'selected':''?>>Aktif</option>
                                                <option <?= $pegawai->statuspegawai=='Pensiun' ? 'selected':''?>>Pensiun</option>
                                                <option <?= $pegawai->statuspegawai=='Kontrak' ? 'selected':''?>>Kontrak</option>
                                                <option <?= $pegawai->statuspegawai=='Mengundurkan Diri' ? 'selected':''?>>Mengundurkan Diri</option>
                                            </select>
                                        </div>
                                    </div>          
                                    <div class="form-group">
                                        <b>Nama</b>
                                        <div class="form-line">
                                            <input required type="text" class="form-control" name="nama" value="<?= ucwords($pegawai->nama)?>">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label>Gelar Depan</label>
                                        <div class="form-line"> 
                                            <input type="text" name="gelardepan" class="form-control" value="<?=$pegawai->gelardepan?>">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label>Gelar Belakang</label>
                                        <div class="form-line"> 
                                            <input type="text" name="gelarbelakang" class="form-control" value="<?=$pegawai->gelarbelakang?>">
                                        </div>
                                    </div>                                    
                                    <div class="form-group">
                                        <b>Kategori</b>
                                        <select required class="form-control show-tick" data-live-search="true" name="kategori">
                                            <option></option> 
                                            <option <?php if(trim($pegawai->kategori)=='kependidikan'){echo "selected";}?> value="kependidikan" >Tendik</option>
                                            <option <?php if(trim($pegawai->kategori)=='pendidik'){echo "selected";}?> value="pendidik">Pendidik</option>
                                        </select>
                                    </div>
                                    <div class="form-group">
                                        <label>Awal Masuk</label>
                                        <div class="form-line">
                                            <input type="text" name="awalmasuk" class="form-control datepicker" value="<?= date('d-m-Y',strtotime($pegawai->awalmasuk))?>">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label>Dianggkat Tetap</label>
                                        <div class="form-line">
                                            <input type="text" name="awaltetap" class="form-control datepicker" value="<?= date('d-m-Y',strtotime($pegawai->awaltetap))?>">
                                        </div>
                                    </div>                                     
                                </div>
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <b>Status</b>
                                        <select required class="form-control show-tick" name="status" data-live-search="true" data-size="7">
                                         <optgroup label="PENDIDIK/DOSEN">   
                                            <option></option> 
                                            <option <?php if(trim($pegawai->idstatuspeg)=='Dosen Tetap')echo "selected";?> value="Dosen Tetap">Dosen Tetap</option>
                                            <option <?php if(trim($pegawai->idstatuspeg)=='PNS DPK')echo "selected";?> value="PNS DPK">PNS DPK</option>
                                            <option <?php if(trim($pegawai->idstatuspeg)=='Tidak Tetap')echo "selected";?>value="Tidak Tetap">Tidak Tetap</option>   
                                            <option <?php if(trim($pegawai->idstatuspeg)=='DPPK')echo "selected";?> value="DPPK">DPPK</option>
                                        </optgroup>
                                        <optgroup label="KEPENDIDIKAN">    
                                            <option <?php if(trim($pegawai->idstatuspeg)=='RT')echo "selected";?> value="RT">RT</option>
                                            <option <?php if(trim($pegawai->idstatuspeg)=='Satpam')echo "selected";?> value="Satpam">Satpam</option>
                                            <option <?php if(trim($pegawai->idstatuspeg)=='Pengemudi')echo "selected";?> value="Pengemudi">Pengemudi</option>
                                            <option <?php if(trim($pegawai->idstatuspeg)=='Laboran')echo "selected";?> value="Laboran">Laboran</option>
                                            <option <?php if(trim($pegawai->idstatuspeg)=='Administrasi')echo "selected";?> value="Administrasi">Administrasi</option>
                                            <option <?php if(trim($pegawai->idstatuspeg)=='Teknisi')echo "selected";?> value="Teknisi">Teknisi</option>                                                  
                                        </optgroup>    
                                        </select>
                                    </div>                                                                          
                                    <div class="form-group">
                                        <b>Penempatan</b>
                                        <select required class="form-control show-tick" data-live-search="true" name="penempatan" data-size="10">
                                            <option></option> 
                                            <?php
                                                foreach ($department as $val) {
                                                    echo "<option value='".$val->departmentname."'";
                                                        if(trim($pegawai->idunitkerja)==$val->departmentname){echo " selected";}
                                                    echo ">".ucwords($val->departmentname)."</option>";
                                                }
                                            ?>
                                        </select>
                                    </div>                                    
                                    <div class="form-group">
                                        <b>Golongan</b>
                                        <select required class="form-control show-tick" name="golongan" data-live-search="true" data-size="7">
                                            <option></option> 
                                            <?php
                                                foreach ($golongan as $value) {
                                                    echo "<option value='".$value->pangkat."'";
                                                        if($pegawai->idgolongan==$value->pangkat){echo "selected";}
                                                    echo ">".$value->pangkat." / ".$value->golongan." ".$value->ruang."</option>";
                                                }    
                                            ?>                                            
                                        </select>
                                    </div>  
                                    <div class="form-group">
                                        <b>Jabatan</b>
                                        <select required class="form-control show-tick" name="jabatan" data-live-search="true" data-size="7">     
                                        <option></option>                                        
                                        <option <?php if($pegawai->jabatan=="Asistan Ahli")?> value="Asisten Ahli">Asisten Ahli</option>
                                        <option <?php if($pegawai->jabatan=="Lektor")?> value="Lektor">Lektor</option>
                                        <option <?php if($pegawai->jabatan=="Lekror Kepala")?> value="Lektor Kepala">Lektor Kepala</option>
                                        <option <?php if($pegawai->jabatan=="Guru Besar")?> value="Guru Besar">Guru Besar</option>                                       
                                    </select>
                                    </div>                                                                  
                                    <div class="form-group">
                                        <b>Gaji Terakhir</b>
                                        <div class="form-line">
                                            <input type="text" class="form-control price" name="gajiterakhir" value="<?php echo $pegawai->gajiawal?>">
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <b>File Foto</b>
                                        <input type="file" name="filefoto">
                                        <p class="label label-primary">File tersimpan :<?php echo $pegawai->foto?></p>
                                    </div> 
                                    <div class="form-group hide">
                                        <b>Foto Terakhir</b>
                                        <div class="form-line">
                                            <input type="text" class="form-control" name="fotolama" value="<?php echo $pegawai->foto?>">
                                        </div>
                                    </div>                                                                        
                                </div>                                
                            </div>
                            <div class="row">
                                <div class="col-sm-12">
                                    <div class="form-group">
                                        <h2 class="card-inside-title bg-blue" style="padding: 10px">Data Diri</h2>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <label>Tempat Lahir</label>
                                        <div class="form-line">
                                            <input type="text" name="tempatlahir" class="form-control" value="<?= $pegawai->tempatlahir?>">
                                        </div>
                                    </div>                                    
                                    <div class="form-group">
                                        <label>Tanggal Lahir</label>
                                        <div class="form-line">
                                            <input type="text" name="tanggallahir" class="form-control datepicker" value="<?= date('d-m-Y',strtotime($pegawai->tanggallahir))?>">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label>Jenis Kelamin</label>
                                        <div class="demo-radio-button">
                                            <input name="jeniskelamin" type="radio" id="radio_1"  value="Laki-laki" <?= trim($pegawai->idjeniskelamin)=='Laki-laki'? 'checked':''?> />
                                            <label for="radio_1">Laki - laki</label>
                                            <input name="jeniskelamin" type="radio" id="radio_2" value="Perempuan" <?= trim($pegawai->idjeniskelamin)=='Perempuan'? 'checked':''?>/>
                                            <label for="radio_2">Perempuan</label>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label>Agama</label>
                                        <div class="form-line">
                                            <select required class="form-control show-tick" data-live-search="true" name="agama">
                                                <option></option> 
                                                <option value="Budha" <?=trim($pegawai->idagama)=='Budha'?'selected':''?>>Budha</option>
                                                <option value="Islam" <?=trim($pegawai->idagama)=='Islam'?'selected':''?>>Islam</option>
                                                <option value="Hindu" <?=trim($pegawai->idagama)=='Hindu'?'selected':''?>>Hindu</option>
                                                <option value="katolik" <?=trim($pegawai->idagama)=='Katolik'?'selected':''?>>Katolik</option>
                                                <option value="Kristen" <?=trim($pegawai->idagama)=='Kristen'?'selected':''?>>Kristen</option>
                                            </select>                                            
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label>Status Nikah</label>
                                        <div class="form-line">
                                        <select required class="form-control show-tick" data-live-search="true" name="statusnikah">
                                            <option></option> 
                                            <option value="Belum Nikah" <?= trim($pegawai->statusnikah)=='Belum Nikah' ? 'selected':'' ?>>Belum Nikah</option>
                                            <option value="Nikah" <?= trim($pegawai->statusnikah)=='Nikah' ? 'selected':'' ?>>Nikah</option>
                                            <option value="Janda/Duda" <?= trim($pegawai->statusnikah)=='Janda/Duda' ? 'selected':'' ?>>Janda/Duda</option>
                                            <option value="Cerai Mati" <?= trim($pegawai->statusnikah)=='Cerai Mati' ? 'selected':'' ?>>Cerai Mati</option>
                                        </select>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label>Golongan Darah</label>
                                        <div class="form-line">
                                           <select required class="form-control show-tick" data-live-search="true" name="goldarah">
                                                <option></option> 
                                                <option value="A" <?=trim($pegawai->idgoldarah)=='A'? 'selected':''?>>A</option>
                                                <option value="B" <?=trim($pegawai->idgoldarah)=='B'? 'selected':''?>>B</option>
                                                <option value="O" <?=trim($pegawai->idgoldarah)=='O'? 'selected':''?>>O</option>
                                                <option value="AB" <?=trim($pegawai->idgoldarah)=='AB'? 'selected':''?>>AB</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label>No HP</label>
                                        <div class="form-line">
                                            <input type="text" name="nohp" class="form-control" value="<?= $pegawai->nohp?>">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label>No KTP</label>
                                        <div class="form-line">
                                            <input type="text" name="noktp" class="form-control" value="<?= $pegawai->noktp?>">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label>Kode POS</label>
                                        <div class="form-line">
                                            <input type="text" name="kodepos" class="form-control " value="<?= $pegawai->kodepos?>">
                                        </div>
                                    </div>                                                        
                                </div>
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <label>Email</label>
                                        <div class="form-line">
                                            <input type="text" name="email" class="form-control" value="<?= $pegawai->email?>">
                                        </div>
                                    </div>                                    
                                    <div class="form-group">
                                        <label>No Kartu Keluarga</label>
                                        <div class="form-line">
                                            <input type="text" name="nokk" class="form-control" value="<?= $pegawai->nohp?>">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label>No BPJS</label>
                                        <div class="form-line">
                                            <input type="text" name="nobpjs" class="form-control" value="<?= $pegawai->nobpjs?>">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label>NPWP</label>
                                        <div class="form-line">
                                            <input type="text" name="npwp" class="form-control" value="<?= $pegawai->nonpwp?>">
                                        </div>
                                    </div>                                                                          
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-12">
                                    <div class="form-group">
                                        <label>Alamat</label>
                                        <div class="form-line">
                                            <textarea class="form-control" rows="4" name="alamat"><?= $pegawai->alamatjalan?></textarea>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-12">
                                    <div class="form-group">
                                        <h2 class="card-inside-title bg-blue" style="padding: 10px">Berkas Upload<br><small>Format PDF ukuran maksimal 5Mb</small></h2>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <label>File SK Masuk</label>
                                        <div class="form-lines">
                                            <input type="file" name="fileksmasuk">
                                            <p class="label label-primary">File tersimpan : <?= $pegawai->fileskmasuk ? $pegawai->fileskmasuk:'Belum upload'?></p>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label>File SK Tetap</label>
                                        <div class="form-lines">
                                            <input type="file" name="filekstetap">
                                            <p class="label label-primary">File tersimpan :<?= $pegawai->filesktetap ? $pegawai->filesktetap:'Belum Upload'?></p>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label>File SK KTP</label>
                                        <div class="form-lines">
                                            <input type="file" name="filektp">
                                            <p class="label label-primary">File tersimpan : <?= $pegawai->filektp ? $pegawai->filektp:'Belum Upload'?></p>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label>File KK</label>
                                        <div class="form-lines">
                                            <input type="file" name="filekk">
                                            <p class="label label-primary">File tersimpan :<?= $pegawai->filekk ? $pegawai->filekk:'Belum Upload'?></p>
                                        </div>
                                    </div>                                    
                                </div>
                            </div>                                                         
                            <div class="row clearfix">
                                <div class="col-sm-12">
                                    <button type="submit" class="btn btn-info btn-block btn-lg waves-effect">Update</button>
                                </div>
                            </div>                        
                       </form> 
                    </div>
                </div>            
            </div>                    			
		</div>
		<!--SAMPAI DISINI-->
	</div>
    <!--DETAIL SEMINAR-->
    <div id="homebase" class="modal fade" role="dialog">
    </div>    
</section>