<section class="content">
	<div class="container-fluid">		
		<div class="block-header">
				<h2><?php echo $headline ?>
				<small>Masa Kerja Golongan, Adalah masa kerja yang dihitung dari TMT SK Tetap</small>
				<small>Masa Kerja Keseluruhan, Adalah masa kerja yang dihitung dari TMT SK Capeg</small>
			</h2>
		</div>
        <!--KONFIRMASI AKSI-->
		<?php include 'notifikasi.php'; ?>                    		
		<div class="row clearfix">
            <div class="col-lg-12 col-md-12">
	            <div class="card">
					<div class="header bg-light-blue">
					    <h2>
					        Masa Bakti<small>Update data pegawai untuk hasil yang valid</small>
					    </h2>
					</div>	
	                <div class="body">
                        <ul class="nav nav-tabs tab-nav-right" role="tablist">
                            <li role="presentation" class="active"><a href="#dosen" data-toggle="tab">Dosen</a></li>
                            <li role="presentation"><a href="#tendik" data-toggle="tab">Tendik</a></li>
                        </ul>	                	
	                   
						<div class="tab-content">
							<div role="tabpanel" class="tab-pane fade in table-responsive active" id="dosen">              
							    <table class="table tabel table-bordered table-striped table-hover" width="100%">
							        <thead>
							        	<tr>
							        		<th width="5%">No</th>
							        		<th width="20%">Nama</th>
							        		<th width="10%">Lahir</th>
							        		<th width="10%">Umur</th>
							        		<th width="10%">Masuk</th>
							        		<th width="10%">Tetap</th>
							        		<th width="10%">MKK</th>	
							        		<th width="10%">MKG</th>
							        		                            		
							        	</tr>
							        </thead>
							        <tbody>
							        	<?php $i=1;foreach($data AS $row):?>
							        	<tr>
							        		<td><?=$i?></td>
							        		<td><?=$row->gelardepan.' '.ucwords($row->nama).' '.$row->gelarbelakang?><br><small class="label label-info"><?= !$row->statuspegawai ? 'Aktif':$row->statuspegawai ?></small></td>
							        		<td><?= date('d-m-Y',strtotime($row->tanggallahir))?></td>
							        		<td><?=$row->umur['tahun'].', '.$row->umur['bulan'].' Bulan'?></td>
							        		<td><?= date('d-m-Y',strtotime($row->awalmasuk))?></td>
							        		<td><?= date('d-m-Y',strtotime($row->awaltetap))?></td>
							        		<td><?=$row->mkk['tahun'].' Th, '.$row->mkk['bulan'].' Bulan'?></td>
							        		<td><?=$row->mkg['tahun'].' Th, '.$row->mkg['bulan'].' Bulan'?></td>
							        	</tr>
							        	<?php $i++;endforeach;?>
							        </tbody>
							    </table>
							</div>
							<div role="tabpanel" class="tab-pane fade in table-responsive" id="tendik">              
							    <table class="table tabel table-bordered table-striped table-hover" width="100%">
							        <thead>
							        	<tr>
							        		<th width="5%">No</th>
							        		<th width="20%">Nama</th>
							        		<th width="10%">Lahir</th>
							        		<th width="10%">Umur</th>
							        		<th width="10%">Masuk</th>
							        		<th width="10%">Tetap</th>
							        		<th width="10%">MKK</th>	
							        		<th width="10%">MKG</th>
							        		                            		
							        	</tr>
							        </thead>
							        <tbody>
							        	<?php $i=1;foreach($tendik AS $row):?>
							        	<tr>
							        		<td><?=$i?></td>
							        		<td><?=$row->gelardepan.' '.ucwords($row->nama).' '.$row->gelarbelakang?><br><small class="label label-info"><?= !$row->statuspegawai ? 'Aktif':$row->statuspegawai ?></small></td>
							        		<td><?= date('d-m-Y',strtotime($row->tanggallahir))?></td>
							        		<td><?=$row->umur['tahun'].', '.$row->umur['bulan'].' Bulan'?></td>
							        		<td><?= date('d-m-Y',strtotime($row->awalmasuk))?></td>
							        		<td><?= date('d-m-Y',strtotime($row->awaltetap))?></td>
							        		<td><?=$row->mkk['tahun'].' Th, '.$row->mkk['bulan'].' Bulan'?></td>
							        		<td><?=$row->mkg['tahun'].' Th, '.$row->mkg['bulan'].' Bulan'?></td>
							        	</tr>
							        	<?php $i++;endforeach;?>
							        </tbody>
							    </table>
							</div>							
						</div>    
	                   	
	               	</div>   	     
	            </card>                        
           	</div>                      			
		</div>
		<!--SAMPAI DISINI-->
	</div>  
</section>
<?php include 'action.php';?>