<style type="text/css">
	table,th,td{
		border:0px solid black;
	}
	th,td{
		padding: 5px;
	}
	.border{
		border:1px solid black;
		border-collapse: collapse;
	}
</style>
<table width="100%" >
	<tr>
		<td  colspan ="3" align="center" height="70px" valign="top"><h2>Data Diri</h2></td>	
	</tr>
	<tr>
		<th width="30%" align="left">Nama</th>
		<td width="50%">
			<?php if((trim($pegawai->gelardepan)!='-') AND ($pegawai->gelardepan)):?>
				<?=$pegawai->gelardepan?>
			<?php endif;?>
			<?php echo " ".ucwords($pegawai->nama)?>
			<?php if((trim($pegawai->gelarbelakang)!='-') AND ($pegawai->gelarbelakang)):?>
				<?=', '.$pegawai->gelarbelakang?>
			<?php endif;?>			
		</td>
		<td align="left" width="20%" rowspan="10">
			<img src="<?= base_url('berkas/fileFoto/'.$pegawai->foto)?>" width='160px' height="200px">
		</td>
	</tr>
	<tr>
		<th  align="left">Jabatan</th>
		<td><?= ucwords($pegawai->jabatan)?></td>
	</tr>
	<tr>
		<th  align="left">Golongan</th>
		<td><?= ucwords($pegawai->idgolongan)?></td>
	</tr>
	<tr>
		<th  align="left">Pendidikan Terakhir</th>
		<td><?= ucwords($pegawai->idpendidikantertinggi)?></td>
	</tr>
	<tr>
		<th  align="left">Tanggal Lahir</th>
		<td><?= !empty($pegawai->tanggallahir)? date('d-m-Y',strtotime($pegawai->tanggallahir)):'-'?></td>
	</tr>	
	<tr>
		<th  align="left">Jenis Kelamin</th>
		<td><?= ucwords($pegawai->idjeniskelamin)?></td>
	</tr>				
	<tr>
		<th  align="left">Agama</th>
		<td><?= ucwords($pegawai->idagama)?></td>
	</tr>
	<tr>
		<th  align="left">Status Nikah</th>
		<td><?= ucwords($pegawai->statusnikah)?></td>
	</tr>
	<tr>
		<th  align="left">Agama</th>
		<td><?= ucwords($pegawai->idagama)?></td>
	</tr>
	<tr>
		<th  align="left">No.Hp</th>
		<td ><?= ucwords($pegawai->nohp)?></td>
	</tr>
	<tr>
		<th  align="left">Email</th>
		<td colspan="2"><?=$pegawai->email?></td>
	</tr>
	<tr>
		<th  align="left">Alamat</th>
		<td colspan="2"><?= ucwords($pegawai->alamatjalan)?></td>
	</tr>						
</table>
<h3>Riwayat Pendidikan</h3>
<table width="100%" class="border">
	<tr style="background-color:grey;">
		<th width="10%"  class="border" style="color:#fffaf0">No</th>
		<th width="30%"  class="border" style="color:#fffaf0">Jenjang</th>
		<th width="60%"  class="border" style="color:#fffaf0">Sekolah</th>
	</tr>
	<?php $i=1;foreach($pendidikan AS $row):?>
		<tr>
			<td  class="border"><?= $i ?></td>
			<td  class="border"><?= ucwords($row->idjenjang)?></td>
			<td  class="border"><?= ucwords($row->namasekolah)?></td>
		</tr>
	<?php $i++;endforeach;?>
</table>
<?php if(count($abdimas)<>0):?>
<h3>Riwayat Abdimas</h3>
<table width="100%" class="border">
	<tr style="background-color:grey;">
		<th width="10%"  class="border" style="color:#fffaf0">No</th>
		<th width="30%"  class="border"  style="color:#fffaf0">Abdimas</th>
		<th width="60%"  class="border"  style="color:#fffaf0">Lokasi</th>
	</tr>
	<?php $i=1;foreach($abdimas AS $row):?>
		<tr>
			<td  class="border"><?= $i ?></td>
			<td  class="border"><?= ucwords($row->tema)?></td>
			<td  class="border"><?= ucwords($row->lokasi)?></td>
		</tr>
	<?php $i++;endforeach;?>
</table>
<?php endif;?>
<?php if(count($seminar)<>0):?>
<h3>Riwayat Seminar</h3>
<table width="100%" class="border">
	<tr style="background-color:grey;">
		<th width="10%"  class="border" style="color:#fffaf0">No</th>
		<th width="30%"  class="border"  style="color:#fffaf0">Seminar</th>
		<th width="60%"  class="border"  style="color:#fffaf0">Makalah</th>
	</tr>
	<?php $i=1;foreach($seminar AS $row):?>
		<tr>
			<td  class="border"><?= $i ?></td>
			<td  class="border"><?= ucwords($row->namaseminar)?></td>
			<td  class="border"><?= ucwords($row->judulmakalah)?></td>
		</tr>
	<?php $i++;endforeach;?>
</table>
<?php endif;?>
<?php if(count($penelitian)<>0):?>
	<h3>Riwayat Penelitian</h3>	
	<table width="100%" class="border">
		<tr style="background-color:grey;">
			<th width="10%"  class="border" style="color:#fffaf0">No</th>
			<th width="90%"  class="border"  style="color:#fffaf0">Penelitian</th>
		</tr>
		<?php $i=1;foreach($penelitian AS $row):?>
			<tr>
				<td  class="border"><?= $i ?></td>
				<td  class="border"><?= ucwords($row->judulpenelitian)?></td>
			</tr>
		<?php $i++;endforeach;?>
	</table>	
<?php endif;?>
