<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
	<div class="card" >
		<div class="header">
            <h2>
               Riwayat Karir
            </h2>
            <ul class="header-dropdown m-r--5">
                    <li class="dropdown">
                        <a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
                            <i class="material-icons">more_vert</i>
                        </a>
                    </li>
             </ul>				
		</div>
		<div class="body">
			<div class="row clearfix">
				<div class="col-sm-12">
					<div class="table-responsive">
						<table class="table table-bordered table-striped table-hover tabelsimple" style="width:100%">
							<thead class="bg-blue">
								<tr>
									<th width="5%">No</th>
									<th width="20%">Karir</th>
									<th width="60%">Periode</th>
									<th class="text-center" width="15%">Aksi</th>
								</tr>
							</thead>
							<tbody>
								<?php $i=1;foreach($data AS $row):?>
									<tr>
										<td><?= $i?></td>
										<td><?= $row->karir_karir?></td>
										<td><?= date('d-m-Y',strtotime($row->karir_tanggalditerima)).' - '.date('d-m-Y',strtotime($row->karir_purnajabatan))?> </td>
										<td class="text-center">
											<?php require_once 'button.php';?>
										</td>
									</tr>
								<?php $i++;endforeach;?>				
							</tbody>
						</table>
						<p>Keterangan :</p>
						<a href='#' style="width:30px; margin:2px" class='btn btn-xs btn-danger waves-effect'><i class='material-icons'>delete</i></a> : Tombol Hapus <br>
						<a href='#' style="width:30px;margin:2px" class='btn btn-xs btn-warning waves-effect'><i class='material-icons'>mode_edit</i></a> : Tombol Edit <br>								
					</div>
				</div>
			</div>
		</div>
	</div>
</div>