<form id="formorganisasi" action="<?php echo base_url('Organisasi/update')?>" method="POST" enctype="multipart/form-data">
	<h2 class="card-inside-title">Form Keanggotaan Organisasi</h2>
	<div class="row clearfix">
		<div class="col-sm-12">
			<div class="form-group form-float hide ">
				<div class="form-line">
					<input type="text" class="form-control" name="id" value="<?= $organisasi->id ?>">
					<label class="form-label">Id</label>
				</div>
			</div>								
			<div class="form-group form-float hide">
				<div class="form-line">
					<input type="text" class="form-control" name="idpeg" value="<?= $organisasi->idpeg ?>">
					<label class="form-label">Id Pegawai</label>
				</div>
			</div>								
			<div class="form-group">
                <p>
                    <b>Tahun Akademik</b>
                </p>
                <select required name="thnakademik" class="form-control show-tick" data-size="5" data-live-search="true" title="Pilih Tahun Akademik">
                	<?php
                		foreach ($thnakademik as $value) {
                			echo "<option value='".$value->tahun."'";
                				if($organisasi->thnakademik==$value->tahun){echo "selected";}
                			echo ">".$value->tahun."</option>";
                		}
                	?>
                </select>

			</div>
			<div class="form-group form-float">
				<div class="form-line">
					<textarea required name="organisasi" rows="4" class="form-control"><?= ucwords($organisasi->organisasi)?></textarea>
					<label class="form-label">Nama Organisasi</label>
				</div>
			</div>	                            		
        	<div class="form-group form-float">
        		<div class="form-line">
        			<input required name="tglberlaku" type="text" class="form-control datepicker" value="<?= date('d-m-Y',strtotime($organisasi->masaberlaku))?>">
        		</div>
        	</div>
        	<div class="form-group form-float hide">
        		<div class="form-line">
        			<input required name="filelama" type="text" class="form-control datepicker" value="<?=$organisasi->file?>">
        			<label class="form-label">File Lama</label>
        		</div>
        	</div>		                            	                           	
			<h2 class="card-inside-title">Upload </h2>
			<div class="form-group form-float">
				<div class="">
					<input type="file" name="fileupload">
					<p class="help-block col-red">Ukuran Maksimal 5mb, format PDF</p>
				</div>
			</div>									
		</div>
	</div>
	<div class="row clearfix">
		<div class="col-sm-12">
			<button type="submit" class="btn btn-primary btn-lg waves-effect btn-block">Update</button>
			<button type="button" class="tutup_card btn btn-danger btn-lg waves-effect btn-block">Tutup</button>
		</div>
	</div>							 	
</form>
<script type="text/javascript">
	$(".tutup_card").click(function(){
        $("#edit_card").hide();
        $("#tampildata").show();
    })
</script>