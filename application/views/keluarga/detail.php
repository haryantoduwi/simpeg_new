<div class="modal-dialog" role="document">
    <div class="modal-content">
        <div class="modal-header">
            <h4 class="modal-title" id="defaultModalLabel">Data Keluarga</h4>
        </div>
        <div class="modal-body">
            <table class="table table-bordered table-striped" width="100%">
                <tr>
                    <th width="40%">Hubungan</th>
                    <td width="60%"><?= $data->hubungan?></td>
                </tr>
                <tr>
                    <th>Anggota</th>
                    <td><?= ucwords($data->anggota)?></td>
                </tr>
                <tr class="<?= trim($data->hubungan)!='Anak Kandung' ? 'hide':''?> ">
                    <th>Anak ke</th>
                    <td><?= ucwords($data->anak_ke)?></td>
                </tr>
                <tr class="<?= trim($data->hubungan)!='Istri' OR trim($data->hubungan)!='Suami' ? 'hide':''?> ">
                    <th>Tgl Menikah</th>
                    <td><?= $data->tgl_menikah ? date('d-m-Y',strtotime($data->tgl_menikah)):'-'?></td>
                </tr> 
                <tr>
                    <th>Tgl. Lahir</th>
                    <td><?= ucwords($data->kotakelahiran).', '.date('d-m-Y',strtotime($data->tgl_lahir))?></td>
                </tr>
                <tr>
                    <th>No. Tlp</th>
                    <td><?= trim($data->telp)?></td>
                </tr>
                <tr>
                    <th>Alamat</th>
                    <td><?= ucwords($data->alamat)?></td>
                </tr> 
                <tr>
                    <th>Pekerjaan</th>
                    <td><?= ucwords($data->pekerjaan)?></td>
                </tr>                                                                                                               
            </table>
        </div>
        <div class="modal-footer">
            <button type="button" class="btn btn-info waves-effect btn-block" data-dismiss="modal">Tutup</button>
        </div>
    </div>
</div>  