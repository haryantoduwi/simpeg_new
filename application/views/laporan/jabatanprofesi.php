<style type="text/css">
	.biru{
		font-weight: 800;
	}
</style>
<section class="content">
	<div class="container-fluid">
		<?php require_once 'notifikasi.php';?>
		<div class="block-header">
			<h2><?= ucwords($judul)?> <small>Riwayat jabatan yang ditampilkan adalah jabatan yang terakhir</small></h2>
		</div>		
		<div class="row clearfix" id="tampildata">
			<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
				<div class="card" >
					<div class="header bg-blue">
						<h2>
							<?= ucwords($judul)?>
						</h2>
						<ul class="header-dropdown m-r--5">
							<li class="dropdown">
								<a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
									<i class="material-icons">more_vert</i>
								</a>
							</li>
						</ul>				
					</div>
					<div class="body">
						<div class="tab-content">
							<div role="tabpanel" class="tab-pane fade in table-responsive active" id="dosen"> 			
								<div class="table-responsive">
									<table class="table table-bordered table-striped table-hover tabelsimple" style="width:100%">
										<thead class="bg-blue">
											<tr>
												<th width="5%">No</th>
												<th width="10%">Param</th>
												<th width="25%">Nama</th>
												<th width="25%">Nomor Sk</th>
												<th width="5%">file</th>
											</tr>
										</thead>
										<tbody>
											<?php $i=1;foreach($dt AS $row): ?>
												<tr class="<?= $row->idjabatan? 'bg-green':'bg-red'?>">
													<td><?=$i?></td>
													<td><?= $row->idpeg?></td>
													<td><?=($row->gelardepan ? $row->gelardepan.', ':'').$row->nama.($row->gelarbelakang ? ', '.$row->gelarbelakang:'')?></td>
													<td class="biru"><?= $row->nomorsk?></td>
													<td> 
														<a url='<?= site_url('Laporandetail/detailjabatanprofesi/')?>' onclick="detailmodal(<?=$row->idpeg?>)" class='detaillaporan  btn btn-xs waves-effect  <?=$row->idjabatan ? "btn-info":"disabled btn-warning"?>'><i class='material-icons'>visibility</i></a> 
													</td>
												</tr>
											<?php $i++;endforeach;?>
										</tbody>
									</table>						
									<?php //print_r($dt)?>		
								</div>
							</div>
						</div>		
					</div>
				</div>
			</div>		
		</div>		
	</div>			
<div class="modal fade" id="detail" tabindex="-1"></div>				
</section>
<?php require_once('action.php') ?>