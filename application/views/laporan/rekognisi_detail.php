<div class="modal-dialog">  
  <div class="modal-content">
    <div class="modal-header">
      <button type="button" class="close" data-dismiss="modal" aria-label="Close">
        <span aria-hidden="true">&times;</span></button>
        <h1 class="modal-title">Detail Rekognisi</h1>
      </div>
      <table class="table table-bordered table-hover">
        <tr>
          <th>No</th>
          <th>Nama rekognisi</th>
          <th>Tempat</th>
          <th>Tanggal</th>
          <th>File</th>
        </tr>
        <?php
        $no=1;
        foreach ($rekognisi as $dt) {
          ?>
          <tr>
            <td><?=$no++?></td>
            <td><?=$dt->rekognisi_rekognisi?></td>
            <td><?=$dt->rekognisi_tempat?></td>
            <td><?=$dt->rekognisi_tglkegiatan?></td>
            <td><a href='<?= site_url('Rekognisi/downloadfile/'.$dt->rekognisi_file)?>' class='btn btn-xs btn-primary waves-effect'><i class='material-icons'>file_download</i></a> </td>
          </tr>
          <?php
        }
        ?>
      </table>
    </div>
  </div> 