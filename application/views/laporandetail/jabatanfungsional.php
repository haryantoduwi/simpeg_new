<div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
        <div class="modal-header bg-green">
            <h4 class="modal-title" id="largeModalLabel"><?= ucwords($judul)?></h4>
        </div>
        <div class="modal-body">
            <table width="100%" class="table table-striped">
                <tr class="bg-blue">
                    <td width="5%">No</td>
                    <td width="30%">Nama</td>
                    <td width="15%">TMT</td>
                    <td width="15%">JAFA</td>
                    <td width="15%">Angka Kredit</td>
                    <td width="10%">File</td>
                </tr>
                <!---->                
                <?php $i=1;foreach($data AS $row):?>
                    <tr>
                        <td><?= $i?></td>
                        <td><?=($row->gelardepan ? $row->gelardepan.', ':'').ucwords($row->nama).($row->gelarbelakang ? ', '.$row->gelarbelakang:'')?></td>
                        <td><?=date('d-m-Y',strtotime($row->tmt))?></td>
                        <td><?= ucwords($row->namajafa)?></td>
                        <td><?= ucwords($row->angkakredit)?></td>
                        <td><a href="<?= base_url('Laporandetail/detailjabatanfungsional/'.$row->file)?>" class="btn btn-xs btn-success <?=$row->file ? "":"hide"?>">Download</a></td>
                    </tr>
                <?php $i++;endforeach;?>
                
            </table>
            <?php
                //print_r($data);
            ?>
        </div>
        <div class="modal-footer">
            <button type="button" class="btn btn-danger waves-effect btn-block btn-lg" data-dismiss="modal">Tutup</button>
        </div>
    </div>
</div>