<div class="modal-dialog modal-lg" role="document" style="max-width:1200px;width: 100%">
    <div class="modal-content">
        <div class="modal-header bg-green">
            <h4 class="modal-title" id="largeModalLabel"><?= ucwords($judul)?></h4>
        </div>
        <div class="modal-body">
            <table width="100%" class="table table-striped">
                <tr class="bg-blue">
                    <td width="5%">No</td>
                    <td width="20%">Nama</td>
                    <td width="20%">Golongan</td>
                    <td width="30%">Nomor SK</td>
                    <td width="15%">TMT</td>
                    <td width="10%">File</td>
                </tr>
                <!---->               
                <?php $i=1;foreach($data AS $row):?>
                    <tr>
                        <td><?= $i?></td>
                        <td><?=($row->gelardepan ? $row->gelardepan.', ':'').ucwords($row->nama).($row->gelarbelakang ? ', '.$row->gelarbelakang:'')?></td>
                        <td><?=$row->namagol?></td>
                        <td><?=$row->nomorsk?></td>
                        <td><?=date('d-m-Y',strtotime($row->tmt))?></td>
                        <td> <a href="<?= base_url('Laporandetail/detailjabatanprofesi/'.$row->file)?>" class="btn btn-xs btn-success <?=$row->file ? "":"hide"?>">Download</a></td>
                    </tr>
                <?php $i++;endforeach;?>
                 
            </table>

            <?php
                //print_r($data);
            ?>
        </div>
        <div class="modal-footer">
            <button type="button" class="btn btn-danger waves-effect btn-block btn-lg" data-dismiss="modal">Tutup</button>
        </div>
    </div>
</div>