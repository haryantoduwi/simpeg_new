<div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
        <div class="modal-header bg-green">
            <h4 class="modal-title" id="largeModalLabel"><?= ucwords($judul)?></h4>
        </div>
        <div class="modal-body">
            <table width="100%" class="table table-striped">
                <tr class="bg-blue">
                    <td width="5%">No</td>
                    <td width="10%">Tahun</td>
                    <td width="30%">Jurnal</td>
                    <td width="30%">Makalah</td>
                    <td width="25%">Lembaga</td>
                </tr>
                <!---->                
                <?php $i=1;foreach($data AS $row):?>
                    <tr>
                        <td><?= $i?></td>
                        <td><?=$row->thakademik?></td>
                        <td><?=ucwords($row->namajurnal)?></td>
                        <td><?=ucwords($row->judmakalah)?></td>
                        <td><?=ucwords($row->lembaga)?> <a href="<?= base_url('Laporandetail/detailjurnal/'.$row->file)?>" class="btn btn-xs btn-success">Download</a></td>
                    </tr>
                <?php $i++;endforeach;?>
                
            </table>
            <?php
                //print_r($data);
            ?>
        </div>
        <div class="modal-footer">
            <button type="button" class="btn btn-danger waves-effect btn-block btn-lg" data-dismiss="modal">Tutup</button>
        </div>
    </div>
</div>