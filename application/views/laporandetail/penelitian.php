<div class="modal-dialog modal-lg" role="document" style="max-width:1200px;width: 100%">
    <div class="modal-content">
        <div class="modal-header bg-green">
            <h4 class="modal-title" id="largeModalLabel"><?= ucwords($judul).', '.$nama->gelardepan.' '.ucwords($nama->nama).' '.$nama->gelarbelakang ?></h4>
        </div>
        <div class="modal-body">
            <table width="100%" class="table table-striped">
                <tr class="bg-blue">
                    <td width="5%">No</td>
                    <td width="10%">Tahun</td>
                    <td width="30%">Penelitian</td>
                    <td width="20%">Skema</td>
                    <td width="35%">Kompetensi</td>
                </tr>
                <!-- -->               
                <?php $i=1;foreach($data AS $row):?>
                    <tr>
                        <td><?= $i?></td>
                        <td><?=$row->tahunakademik?></td>
                        <td><?=$row->judulpenelitian?></td>
                        <td><?=$row->skema?></td>
                        <td><?=$row->file?> <a href="<?= base_url('Laporandetail/detailpenelitian/'.$row->file)?>" class="btn btn-xs btn-success <?=$row->file ? "":"hide"?>">Download</a></td>
                    </tr>
                <?php $i++;endforeach;?>
                
            </table>
            <?php
                //print_r($data);
            ?>
        </div>
        <div class="modal-footer">
            <button type="button" class="btn btn-danger waves-effect btn-block btn-lg" data-dismiss="modal">Tutup</button>
        </div>
    </div>
</div>