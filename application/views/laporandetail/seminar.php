<div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
        <div class="modal-header bg-green">
            <h4 class="modal-title" id="largeModalLabel"><?= ucwords($judul)?></h4>
        </div>
        <div class="modal-body">
            <table width="100%" class="table table-striped">
                <tr class="bg-blue">
                    <td width="5%">No</td>
                    <td width="10%">Tahun</td>
                    <td width="40%">Seminar</td>
                    <td width="30%">Penyelenggara</td>
                    <td width="15%">File</td>
                </tr>
                <!---->                
                <?php $i=1;foreach($data AS $row):?>
                    <tr>
                        <td><?= $i?></td>
                        <td><?=$row->tahunakademik?></td>
                        <td><?=ucwords($row->namaseminar)?> <br><small>Pelaksana : <?=date('d-m-Y',strtotime($row->tglpelaksana))?></small></td>
                        <td><?=ucwords($row->penyelenggara)?></td>
                        <td><a href="<?= base_url('Laporandetail/detailseminar/'.$row->file)?>" class="btn btn-xs btn-success <?=$row->file ? "":"hide"?>">Download</a></td>
                    </tr>
                <?php $i++;endforeach;?>
                
            </table>
            <?php
                //print_r($data);
            ?>
        </div>
        <div class="modal-footer">
            <button type="button" class="btn btn-danger waves-effect btn-block btn-lg" data-dismiss="modal">Tutup</button>
        </div>
    </div>
</div>