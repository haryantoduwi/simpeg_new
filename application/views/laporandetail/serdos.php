<div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
        <div class="modal-header bg-green">
            <h4 class="modal-title" id="largeModalLabel"><?= ucwords($judul)?></h4>
        </div>
        <div class="modal-body">
            <table width="100%" class="table table-striped">
                <tr class="bg-blue">
                    <td width="5%">No</td>
                    <td width="35%">Nama</td>
                    <td width="10%">Tahun</td>
                    <td width="45%">File</td>
                </tr>
                <!---->                
                <?php $i=1;foreach($data AS $row):?>
                    <tr>
                        <td><?= $i?></td>
                        <td><?=($row->gelardepan ? $row->gelardepan.', ':'').$row->nama.($row->gelarbelakang ? ', '.$row->gelarbelakang:'')?></td>
                        <td><?=$row->serdos_tahundiperoleh?></td>
                        <td><?=$row->serdos_file?> <a href="<?= base_url('Laporandetail/detailserdos/'.$row->serdos_file)?>" class="btn btn-xs btn-success">Download</a></td>
                    </tr>
                <?php $i++;endforeach;?>
                
            </table>
            <?php
                //print_r($data);
            ?>
        </div>
        <div class="modal-footer">
            <button type="button" class="btn btn-danger waves-effect btn-block btn-lg" data-dismiss="modal">Tutup</button>
        </div>
    </div>
</div>