<section class="content">
	<div class="container-fluid">
		<div class="block-header">
			<h2>RIWAYAT JABATAN</h2>
		</div>
		<!--KONFIRMASI AKSI-->
		<?php
			if($this->session->flashdata('success')){
				echo'
                    <div class="alert alert-success alert-dismissible" role="alert">
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        '.$this->session->flashdata("success").'
                    </div>
				';
			}elseif($this->session->flashdata('error')){
				echo'
                    <div class="alert alert-danger alert-dismissible" role="alert">
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        '.$this->session->flashdata("error").'
                    </div>
				';				
			}	
		?>
		<!--FORM TAMBAH DATA-->
		<div class="row clearfix formtambah">
			<div class="col-sm-2">
				<div class="form-group">
					<button type="button" class="tomboltambah btn btn-primary btn-lg m-t-15 waves-effect btn-block">Tambah</button>
				</div>
			</div>
		</div>			
		<!--FORM TAMBAH DATA-->
		<div class="row clearfix">
			<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
				<div class="card forminput" style="display:none">
					<div class="body">
						<form action="<?php echo site_url('Riwayatjabatan/simpan_data')?>" method="POST" enctype="multipart/form-data" >
							<h2 class="card-inside-title">Riwayat Jabatan Pegawai</h2>
							<div class="row clearfix">
								<div class="col-sm-12">
									<div class="form-group form-float">
										<div class="form-line">
											<input type="text" class="form-control" name="nomorsk">
											<label class="form-label">Nomor SK</label>
										</div>
									</div>
									<div class="form-group form-float">
	                                    <p>
	                                        <b>Golongan</b>
	                                    </p>
	                                    <select required name="golongan" class="form-control show-tick" data-size="5" data-live-search="true" title="Pilih Golongan"> 
	                                    	<?php
	                                    		foreach ($golongan as $value) {
	                                    			echo "<option value='".$value->pangkat."/".$value->golongan." ".$value->ruang."'>".$value->pangkat."/".$value->golongan." ".$value->ruang."</option>";
	                                    		}
	                                    	?>
	                                    </select>
									</div>
									<div class="form-group form-float">
										<div class="form-line">
											<b>
												Tanggal Mulai Ditetapkan
											</b>
											<input required type="text" class="form-control datepicker" name="tmt">
										</div>
									</div>
									<h2 class="card-inside-title">Upload SK</h2>
									<div class="form-group">
										<input type="file" name="fileupload">
										<p class="help-block col-red">Ukuran Maksimal 5mb</p>
									</div>
								</div>
							</div>
							<div class="row clearfix">
								<div class="col-sm-12">
									<button type="submit" class="btn btn-primary btn-lg waves-effect btn-block">SIMPAN</button>
								</div>
							</div>
						</form>
					</div>
				</div>
			</div>
		</div>
		<!--TABEL DATA-->
		<div class="row clearfix">
			<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
				<div class="card" >
					<div class="header">
		                <h2>
                           Daftar Riwayat Jabatan
                        </h2>
                        <ul class="header-dropdown m-r--5">
                                <li class="dropdown">
                                    <a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
                                        <i class="material-icons">more_vert</i>
                                    </a>
                                    <!--
                                    <ul class="dropdown-menu pull-right">
                                        <li><a href="javascript:void(0);">Action</a></li>
                                        <li><a href="javascript:void(0);">Another action</a></li>
                                        <li><a href="javascript:void(0);">Something else here</a></li>
                                    </ul>
                                    -->
                                </li>
                         </ul>				
					</div>
					<div class="body">
						<div class="row clearfix">
							<div class="col-sm-12">
								<div class="table-responsive">
									<table class="table table-bordered table-striped table-hover tabelsimple " style="width:1200px">
										<thead>
											<tr>
												<th  width="15px">No</th>
												<th>Golongan</th>
												<th>Nomor SK</th>
												<th>Tanggal</th>
												<th class="text-center">Option</th>
											</tr>
										</thead>
										<tbody>
											<?php
												$i=1;
												foreach ($riwayatjabatan as $val) {
													echo "<tr>";
														echo "<td>".$i."</td>";
														echo "<td>".$val->namagol."</td>";
														echo "<td>".$val->nomorsk."</td>";
														echo "<td>".date('d-m-Y',strtotime($val->tmt))."</td>";
													echo "<td class='text-center'>
														<div class='btn-group'>
															<a href='".site_url('Riwayatjabatan/hapus_data/'.$val->id)."' class='hapus btn btn-xs btn-danger waves-effect'><i class='material-icons'>delete</i></a> 
															<a href='".site_url('Riwayatjabatan/download/'.$val->file)."' class='btn btn-xs btn-warning waves-effect'><i class='material-icons'>archive</i></a> 
														</div>
													</td>";;
													echo "</tr>"; 
													$i++;
												}
											?>											
										</tbody>
									</table>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>			
	</div>
</section>