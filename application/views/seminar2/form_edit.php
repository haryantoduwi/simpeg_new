<form  action="<?php echo base_url('Seminar2/update')?>" method="POST" enctype="multipart/form-data">
	<div class="row clearfix">
		<div class="col-sm-12">
			<div class="form-group form-float hide">
				<div class="form-line">
					<input type="text"  class="form-control" readonly name="id" value="<?= $data->seminar_id?>">
					<label class="form-label">Id</label>
				</div>
			</div>			
			<div class="form-group hide">
				<label>Pegawai</label>
				<div class="form-line">
                    <input type="text" readonly name="seminar_idpeg" class="form-control" value="<?=$data->seminar_idpeg?>">
				</div>
			</div>
            <div class="form-group">
                <label>Jenis</label>
                <div class="form-line">
                    <select class="form-control" name="seminar_jenis" data-size="10" data-live-search="true">
                        <option <?=$data->seminar_jenis=='Seminar' ? 'selected':''?>>Seminar</option>
                        <option <?=$data->seminar_jenis=='Pelatihan' ? 'selected':''?>>Pelatihan</option>
                        <option <?=$data->seminar_jenis=='Studi Banding' ? 'selected':''?>>Studi Banding</option>
                        <option <?=$data->seminar_jenis=='Workshop' ? 'selected':''?>>Workshop</option>
                    </select>
                </div>
            </div>              											
            <div class="form-group">
            	<label class="form-label">Nama Kegiatan</label>
            	<div class="form-line">
            		<input type="text" name="seminar_nama" class="form-control" value="<?=$data->seminar_nama?>">
            	</div>
            </div>
            <div class="form-group">
            	<label class="form-label">Tanggal Kegiatan</label>
            	<div class="form-line">
            		<input type="text" name="seminar_tglkegiatan" class="form-control datepicker" value="<?= date('d-m-Y',strtotime($data->seminar_tglkegiatan))?>">
            	</div>
            </div>                        
           <div class="form-group">
            	<label class="form-label">Keikutsertaan</label>
                <div class="form-line">
                    <select class="form-control" name="seminar_keikutsertaan" data-size="10" data-live-search="true">
                        <option <?= $data->seminar_keikutsertaan=='Peserta'? 'selected':''?>>Peserta</option>
                        <option <?= $data->seminar_keikutsertaan=='Pembicara'? 'selected':''?>>Pembicara</option>
                    </select>                               
                </div>
            </div>                         
           <div class="form-group">
            	<label class="form-label">Lembaga/Penyelenggara</label>
            	<div class="form-line">
            		<textarea type="text" name="seminar_lembaga" class="form-control" rows="4"><?=$data->seminar_lembaga?></textarea>
            	</div>
            </div>
            <div class="form-group">
            	<label>Upload File</label>
            	<input type="file" name="fileupload">
            	<p class="help-block">Format file pdf, ukuran max 5mb</p>
                <p class="col-red">File dapat berupa sertifikat atau surat tugas</p>  
                <span class="label label-info">Tersimpan : <?=$data->seminar_file?></span>                          
            </div>                                                                              										
		</div>
	</div>
	<div class="row clearfix">
		<div class="col-sm-12">
			<button type="submit" value="seminar" name="seminar" class="btn btn-warning btn-lg waves-effect btn-block">UPDATE</button>
			<button type="button" class="tutup_input btn btn-danger btn-lg waves-effect btn-block">Tutup</button>
		</div>
	</div>							 	
</form>
<script type="text/javascript">
    $(document).ready(function(){
        $('.datepicker2').bootstrapMaterialDatePicker({
            format: "YYYY",
            //year:true,
            time:false,
            //date:true,
            //monthPicker:true
        });         
    })
</script>