<form  action="<?php echo base_url('serkom2/update')?>" method="POST" enctype="multipart/form-data">
    <div class="row clearfix">
        <div class="col-sm-12">
            <div class="form-group form-float hide">
                <div class="form-line">
                    <input type="text"  class="form-control" readonly name="id" value="<?= $data->serkom_id?>">
                    <label class="form-label">Id</label>
                </div>
            </div>                                                      
            <div class="form-group">
                <p>
                    <b>Tahun Akademik</b>
                </p>
                <select required name="serkom_tahunakademik" class="form-control show-tick" data-size="5" data-live-search="true" title="Pilih Tahun Akademik">
                    <?php foreach ($thnakademik as $row):?>
                        <option value="<?=$row->tahun?>" <?= $row->tahun==$data->serkom_tahunakademik ? 'selected':''?>><?= $row->tahun ?></option>
                    <?php endforeach;?>
                </select>
            </div>
            <div class="form-group form-float hide">
                    <div class="form-line">
                        <input type="text" required class="form-control" name="serkom_idpegawai" value="<?= $data->serkom_idpegawai?>">
                        <label class="form-label">Id Pegawai</label>
                    </div>
                </div>                         
            <div class="form-group form-float">
                <div class="form-line">
                <input required type="text" name="serkom_tahunkeluar" class="form-control datepicker2" value="<?= $data->serkom_tahunkeluar?>">
                <label class="form-label">Tahun Keluar</label>
                </div>
            </div>
            <div class="form-group form-float">
                <div class="form-line">
                <input required type="text" name="serkom_tahunkadaluarsa" class="form-control datepicker2" value="<?= $data->serkom_tahunkadaluarsa?>">
                <label class="form-label">Tahun Kadaluarsa</label>
                </div>
            </div>                        
            <div class="form-group form-float">
                <div class="form-line">
                <textarea type="text" name="serkom_lembaga" class="form-control" rows="3"><?= $data->serkom_lembaga?></textarea>
                <label class="form-label">Lembaga</label>
                </div>
            </div>
            <div class="form-group form-float">
                <div class="form-line">
                <textarea type="text" name="serkom_kompetensi" class="form-control" rows="3"><?= $data->serkom_lembaga?></textarea>
                <label class="form-label">Kompetansi</label>
                </div>
            </div>                        
            <div class="form-group">
                <label>Upload File</label>
                <input type="file" name="fileupload">
                <p class="help-block">Format file pdf, ukuran max 5mb</p>
                <span class="label label-info">Tersimpan :<?=$data->serkom_file?></span>                           
            </div>                                                                                                                      
        </div>
    </div>
    <div class="row clearfix">
        <div class="col-sm-12">
            <button type="submit" value="submit" name="submit" class="btn btn-warning btn-lg waves-effect btn-block">Update</button>
            <button type="button" class="tutup_input btn btn-danger btn-lg waves-effect btn-block">Tutup</button>
        </div>
    </div>  
</form>
<script type="text/javascript">
    $(document).ready(function(){
        $('.datepicker2').bootstrapMaterialDatePicker({
            format: "YYYY",
            //year:true,
            time:false,
            //date:true,
            //monthPicker:true
        });         
    })
</script>