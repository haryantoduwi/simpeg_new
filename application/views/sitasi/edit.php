<form  action="<?php echo base_url('Sitasi/edit')?>" method="POST" enctype="multipart/form-data">
	<h2 class="card-inside-title">Form Sitasi</h2>
	<div class="row clearfix">
		<div class="col-sm-12">
			<div class="form-group form-float hide">
				<div class="form-line">
					<input type="text" class="form-control" readonly name="id" value="<?= $data->sitasi_id?>">
					<label class="form-label">Id Sitasi</label>
				</div>
			</div>								
			<div class="form-group">
                <b>Sitasi</b>
                <select required name="sitasi_kodekategori" class="form-control show-tick" data-size="5" data-live-search="true" title="Kategori Sitasi">
                	<?php
                		foreach ($kategori as $row) {
                			echo "<option value='".$row->kategorisitasi_id."' ".($data->sitasi_kodekategori==$row->kategorisitasi_id ? 'selected':'').">".ucwords($row->kategorisitasi_kategori)."</option>";
                		}
                	?>
                </select>

			</div>
            <div class="input-group form-float">
                <span class="input-group-addon">http://</span>
                <div class="form-line">
                    <input type="text" required class="form-control" placeholder="Url" name="sitasi_url" value="<?=substr($data->sitasi_url,7)?>">
                </div>
            </div>	
            <p class="help-block">Penulisan url tanpa menggunakan http</p>				                            										
		</div>
	</div>
	<div class="row clearfix">
		<div class="col-sm-12">
			<button type="submit" name="submit" value="submit" class="btn btn-primary btn-lg waves-effect btn-block">Update</button>
			<button type="button" class="tutup_card btn btn-danger btn-lg waves-effect btn-block">Tutup</button>
		</div>
	</div>							 	
</form>
<script type="text/javascript">
	$(".tutup_card").click(function(){
        $("#edit_card").hide();
        $("#tampildata").show();
    })
</script>